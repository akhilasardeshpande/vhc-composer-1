package tests

import (
	"fmt"
	"io/ioutil"
	"testing"

	"bitbucket.org/swigy/vhc-composer/conf"
	"bitbucket.org/swigy/vhc-composer/graph"
	"bitbucket.org/swigy/vhc-composer/graph/model"
	"bitbucket.org/swigy/vhc-composer/mocks"
	"github.com/99designs/gqlgen/client"
	"github.com/go-redis/redis/v8"
	"github.com/jarcoal/httpmock"
	"github.com/stretchr/testify/require"
)

func TestMetrics(t *testing.T) {
	mocks.MockGetSessionSuccessResponse(t)
	defer httpmock.Deactivate()
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()
	cartSessionMockData, err := ioutil.ReadFile("../mocks/data/vi_cart_sessions_response.json")
	require.Equal(t, nil, err, "could not fetch CartSession mock data")
	httpmock.RegisterResponder("GET", conf.VIBaseURL+conf.VICartSessionsPath,
		httpmock.NewBytesResponder(200, cartSessionMockData))

	menuSessionMockData, err := ioutil.ReadFile("../mocks/data/vi_menu_sessions_response.json")
	require.Equal(t, nil, err, "could not fetch MenuSessions mock data")
	httpmock.RegisterResponder("GET", conf.VIBaseURL+conf.VIMenuSessionsPath,
		httpmock.NewBytesResponder(200, menuSessionMockData))

	conversionMockData, err := ioutil.ReadFile("../mocks/data/vi_conversion_metrics_response.json")
	require.Equal(t, nil, err, "could not fetch Conversion Metrics mock data")
	httpmock.RegisterResponder("GET", conf.VIBaseURL+conf.VIConversionMetricsPath,
		httpmock.NewBytesResponder(200, conversionMockData))

	newRepeatMockData, err := ioutil.ReadFile("../mocks/data/vi_new_repeat_customer_response.json")
	require.Equal(t, nil, err, "could not fetch NewRepeatCustomer mock data")
	httpmock.RegisterResponder("GET", conf.VIBaseURL+conf.VINewRepeatCustomerPath,
		httpmock.NewBytesResponder(200, newRepeatMockData))

	businessMetrics, err := ioutil.ReadFile("../mocks/data/vi_business_metrics_response.json")
	require.Equal(t, nil, err, "could not fetch mock data")
	httpmock.RegisterResponder("GET", conf.VIBaseURL+conf.VIBusinessMetricsPath,
		httpmock.NewBytesResponder(200, businessMetrics))

	customerSentiments, err := ioutil.ReadFile("../mocks/data/vi_customer_sentiments_response.json")
	require.Equal(t, nil, err, "could not fetch mock data")
	httpmock.RegisterResponder("GET", conf.VIBaseURL+conf.VICustomerSentimentsPath,
		httpmock.NewBytesResponder(200, customerSentiments))

	redisContainer := InitRedisContainer(t)
	redisClient := redis.NewClient(&redis.Options{
		Addr: fmt.Sprintf("%s:%s", redisContainer.Host(), redisContainer.Port()),
	})
	resolvers := graph.GraphqlHandler(redisClient)
	clt := client.New(graph.GetHeadersMiddleware(resolvers))
	query := `query metrics{
				metrics(period:"TODAY", restIds:[12895]){
					cartSessions{period,vendor{type}}
					menuSessions{period}
					newRepeatCustomer{period}
					conversionMetrics{period}
					businessMetrics{totalOrdersCurrent}
					customerSentiments{arrow}
				}
			}`

	var resp struct {
		Data *model.Metrics `json:"metrics"`
	}

	err = clt.Post(query, &resp, client.AddHeader(sessionKey, ""))
	if err != nil {
		t.Error(err)
	}

	require.Equal(t, "TODAY", resp.Data.CartSessions.Period, "Period did not match")
	require.Equal(t, "RESTAURANT", resp.Data.CartSessions.Vendor.Type, "Type did not match")
	require.Equal(t, int64(10), resp.Data.BusinessMetrics.TotalOrdersCurrent, "Order count did not match")
	require.Equal(t, "green_up", resp.Data.CustomerSentiments.Arrow, "Arrow did not match")
}
