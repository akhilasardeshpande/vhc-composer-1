package tests

import (
	"fmt"
	"io/ioutil"
	"testing"

	"bitbucket.org/swigy/vhc-composer/conf"
	"bitbucket.org/swigy/vhc-composer/graph"
	"bitbucket.org/swigy/vhc-composer/graph/generated"
	"bitbucket.org/swigy/vhc-composer/graph/model"
	"bitbucket.org/swigy/vhc-composer/graph/services/rms_service"
	"bitbucket.org/swigy/vhc-composer/mocks"
	"github.com/99designs/gqlgen/client"
	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/go-redis/redis/v8"
	"github.com/jarcoal/httpmock"
	"github.com/stretchr/testify/assert"
)

func TestRMS(t *testing.T) {
	redisContainer := InitRedisContainer(t)
	redisClient := redis.NewClient(&redis.Options{
		Addr: fmt.Sprintf("%s:%s", redisContainer.Host(), redisContainer.Port()),
	})
	resolvers := graph.GraphqlHandler(redisClient)
	graphqlClient := client.New(graph.GetHeadersMiddleware(resolvers))
	GetUserTest(t, redisClient, graphqlClient)
	GetSessionInfoTest(t, redisClient, graphqlClient)
}

func GetUserTest(t *testing.T, redisClient redis.UniversalClient, c *client.Client) {
	mocks.MockGetUser(t)
	defer httpmock.DeactivateAndReset()

	q := `
	query get_user {
		get_user {
		  mobile, permissions,isDotIn,userRole,isLiveOrder,isCallPartner
		  isBillMaskingRestaurant,isFssaiAlert,isGstAlert,isThirtyMfEnabled
		  restaurants {
			rest_id,rest_name,area_id,area_name,city_id,city_name,locality
			assured,rating,enabled,lat_long,
			restMetadata {restaurantId, id, attribute,value}	
	}}}
	`

	var resp struct {
		GetUser *model.UserDetails `json:"get_user"`
	}
	err := c.Post(q, &resp, client.AddHeader(sessionKey, ""))
	if err != nil {
		t.Error(err)
	}
	t.Log(resp.GetUser)
	assert.Equal(t, 3, resp.GetUser.UserRole)
	assert.Equal(t, []int{1, 2, 3, 4, 5}, resp.GetUser.Permissions)
}
func GetSessionInfoTest(t *testing.T, redisClient redis.UniversalClient, c *client.Client) {
	mocks.MockGetSessionSuccessResponse(t)
	defer httpmock.DeactivateAndReset()

	q := `
	query get_session_info {
		get_session_info {
		   permissions,
		   username,
		   restaurants }}
	`

	var resp struct {
		GetSession *model.SessionDetails `json:"get_session_info"`
	}
	err := c.Post(q, &resp, client.AddHeader(sessionKey, ""))
	if err != nil {
		t.Error(err)
	}
	t.Log(resp.GetSession)
	assert.Equal(t, "12895", resp.GetSession.Username)
	assert.Equal(t, []int{1, 2, 3, 4, 5}, resp.GetSession.Permissions)
}

func TestLogin(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	URL := conf.RmsURL + conf.RmsLoginPath
	mockData, err := ioutil.ReadFile("../mocks/data/rms_login.json")
	if err != nil {
		t.Errorf("Failed to read mock data: %v", err)
	}
	httpmock.RegisterResponder("POST", URL,
		httpmock.NewBytesResponder(200, mockData))

	resolvers := graph.Resolver{
		RMSService: rms_service.NewRMSService(),
	}
	c := client.New(graph.GetHeadersMiddleware(handler.NewDefaultServer(generated.NewExecutableSchema(generated.Config{Resolvers: &resolvers}))))

	q := `
	mutation login {
		login(input: { username: "12895", password: "El3ment@ry" }) {
		  access_token,ID,mobile,rid,name,city,userRole,change_password
		  userType,permissions,restaurants {
			rest_id,rest_name,area_id,area_name,city_id,city_name,locality
			assured,rating,enabled,lat_long
			restMetadata {id,restaurantId,attribute,value}
	}}}`

	var resp struct {
		Login *model.LoginResponse `json:"login"`
	}
	err = c.Post(q, &resp)
	if err != nil {
		t.Error(err)
	}
	t.Logf("Response: %v", resp)
	assert.Equal(t, 3, resp.Login.UserRole)
	assert.Equal(t, []int{1, 2, 3, 4, 5}, resp.Login.Permissions)
	assert.Equal(t, "332ee070-e74d-47fe-8b91-13ef5c54feca", resp.Login.AccessToken)
}

func TestLogout(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	URL := conf.RmsURL + conf.RmsLogoutPath
	mockData, err := ioutil.ReadFile("../mocks/data/rms_logout.json")
	if err != nil {
		t.Errorf("Failed to read mock data: %v", err)
	}
	httpmock.RegisterResponder("POST", URL,
		httpmock.NewBytesResponder(200, mockData))

	resolvers := graph.Resolver{
		RMSService: rms_service.NewRMSService(),
	}
	c := client.New(graph.GetHeadersMiddleware(handler.NewDefaultServer(generated.NewExecutableSchema(generated.Config{Resolvers: &resolvers}))))

	q := `
	mutation logout{
		logout(UserName: "12895" )
	}`

	var resp struct {
		Logout bool `json:"logout"`
	}
	err = c.Post(q, &resp, client.AddHeader(sessionKey, ""))
	if err != nil {
		t.Error(err)
	}
	t.Logf("Response: %v", resp)
	assert.Equal(t, true, resp.Logout)

}
