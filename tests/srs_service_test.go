package tests

import (
	"bitbucket.org/swigy/vhc-composer/graph"
	"bitbucket.org/swigy/vhc-composer/graph/generated"
	"bitbucket.org/swigy/vhc-composer/graph/model"
	"bitbucket.org/swigy/vhc-composer/graph/services/srs_service"
	"bitbucket.org/swigy/vhc-composer/mocks"
	"github.com/99designs/gqlgen/client"
	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/jarcoal/httpmock"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestUpdateSlots(t *testing.T) {
	mocks.MockVendorAuthResponse(t)
	mocks.MockUpdateSlotResponse(t)
	defer httpmock.DeactivateAndReset()

	mutation := `
	mutation updateSlots{
		updateSlots(restaurantId: 9990, input: [{
		day:"Tue",
		slots: [{
		  open_time:"1000",
		  close_time:"2000"
		}
		]
	  }]) { 
		day,
		slots{
		  open_time
		  close_time
		}
	  }
	}
	`
	var resp struct {
		SlotResponse []*model.SlotsByDay `json:"updateSlots"`
	}
	resolvers := graph.Resolver{
		SRSService: srs_service.NewSRSService(),
	}
	c := client.New(graph.GetHeadersMiddleware(handler.NewDefaultServer(generated.NewExecutableSchema(generated.Config{Resolvers: &resolvers}))))

	err := c.Post(mutation, &resp, client.AddHeader(sessionKey, "dummy"))
	if err != nil {
		t.Error(err)
	}
	assert.Equal(t, len(resp.SlotResponse), 2, "Test 1 failed")
	for _, data := range resp.SlotResponse {
		switch data.Day {
		case "Mon":
			assert.Equal(t, len(data.Slots), 2, "Test 2 failed")
			assert.Equal(t, data.Slots[0].OpenTime, 200, "Test 3 failed")
			assert.Equal(t, data.Slots[0].CloseTime, 400, "Test 4 failed")
			assert.Equal(t, data.Slots[1].OpenTime, 1100, "Test 5 failed")
			assert.Equal(t, data.Slots[1].CloseTime, 2000, "Test 6 failed")
		case "Tue":
			assert.Equal(t, len(data.Slots), 1, "Test 7 failed")
			assert.Equal(t, data.Slots[0].OpenTime, 1100, "Test 8 failed")
			assert.Equal(t, data.Slots[0].CloseTime, 2000, "Test 9 failed")
		default:
			t.Error(data.Day)
		}
	}
}

func TestGetSlots(t *testing.T) {
	mocks.MockVendorAuthResponse(t)
	mocks.MockGetSlotResponse(t)
	defer httpmock.DeactivateAndReset()

	query := `
		query getSlots{
			getSlots(
				input: {
				  restaurantIds: [9990]
				}
			){
				rId,
				days {
				  day
				  slots{
					open_time
					 close_time
				  }
				}
			  }
		}
		`
	var resp struct {
		SlotResponse []*model.GetSlotResponse `json:"getSlots"`
	}
	resolvers := graph.Resolver{
		SRSService: srs_service.NewSRSService(),
	}
	c := client.New(graph.GetHeadersMiddleware(handler.NewDefaultServer(generated.NewExecutableSchema(generated.Config{Resolvers: &resolvers}))))

	err := c.Post(query, &resp, client.AddHeader(sessionKey, "dummy"))
	if err != nil {
		t.Error(err)
	}
	assert.Equal(t, 2, len(resp.SlotResponse))
	for _, data := range resp.SlotResponse {
		switch data.RID {
		case 9990:
			for _, d := range data.Days {
				switch d.Day {
				case "Mon":
					assert.Equal(t, len(d.Slots), 2, "Test 2 failed")
					assert.Equal(t, d.Slots[0].OpenTime, 200, "Test 3 failed")
					assert.Equal(t, d.Slots[0].CloseTime, 400, "Test 4 failed")
					assert.Equal(t, d.Slots[1].OpenTime, 600, "Test 5 failed")
					assert.Equal(t, d.Slots[1].CloseTime, 800, "Test 6 failed")
				case "Thu":
					assert.Equal(t, len(d.Slots), 2, "Test 7 failed")
					assert.Equal(t, d.Slots[0].OpenTime, 0, "Test 8 failed")
					assert.Equal(t, d.Slots[0].CloseTime, 200, "Test 9 failed")
					assert.Equal(t, d.Slots[1].OpenTime, 1200, "Test 10 failed")
					assert.Equal(t, d.Slots[1].CloseTime, 2200, "Test 11 failed")
				default:
					t.Error(d.Day)
				}
			}
		case 3456:
			for _, d := range data.Days {
				switch d.Day {
				case "Mon":
					assert.Equal(t, len(d.Slots), 1, "Test 12 failed")
					assert.Equal(t, d.Slots[0].OpenTime, 0, "Test 13 failed")
					assert.Equal(t, d.Slots[0].CloseTime, 200, "Test 14 failed")
				case "Tue":
					assert.Equal(t, len(d.Slots), 1, "Test 15 failed")
					assert.Equal(t, d.Slots[0].OpenTime, 0, "Test 16 failed")
					assert.Equal(t, d.Slots[0].CloseTime, 200, "Test 17 failed")
				default:
					t.Error(d.Day)
				}
			}
		default:
			t.Error(data.RID)
		}
	}
}
