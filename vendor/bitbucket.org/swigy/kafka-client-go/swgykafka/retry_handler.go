package swgykafka

type retryHandler struct {
	delegate        ConsumerHandler
	consumerGroupId string
}

func (r *retryHandler) Handle(record *Record) (Status, error) {
	if !r.isOwner(record) {
		return Success, nil
	}
	return r.delegate.Handle(record)
}

func (r *retryHandler) isOwner(record *Record) bool {
	if record.Headers == nil {
		return false
	}
	consumerId := record.Headers[retryConsumerConsumerIdKey]
	return consumerId != "" && r.consumerGroupId == consumerId
}

func newRetryHandler(delegate ConsumerHandler, consumerGroupId string) *retryHandler {
	return &retryHandler{delegate: delegate, consumerGroupId: consumerGroupId}
}
