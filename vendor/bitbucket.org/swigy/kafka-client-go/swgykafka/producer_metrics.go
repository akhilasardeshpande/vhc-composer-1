package swgykafka

import (
	"github.com/confluentinc/confluent-kafka-go/kafka"
	"github.com/prometheus/client_model/go"
)

//TODO
// producer_topic_metrics_compression_rate
// producer_metrics_connection_creation_rate
// producer_metrics_connection_close_rate
// producer_metrics_select_rate
// producer_metrics_io_ratio
// producer_metrics_io_wait_time_ns_avg
// producer_metrics_io_wait_ratio
// producer_metrics_io_time_ns_avg
// producer_metrics_metadata_age
// producer_metrics_buffer_available_bytes
var producerMetricConverters = metrics{
	{
		//Total number of bytes transmitted for txmsgs by topic
		name:  "go_producer_topic_metrics_byte_total",
		mType: io_prometheus_client.MetricType_GAUGE,
		converter: &functionMetricConverter{func(stats stats) ([]metricValue, error) {
			var metricValues []metricValue
			for topicName, topicMetrics := range stats.Topics {
				totalBytes := float64(0)
				for _, partitionMetrics := range topicMetrics.Partitions {
					totalBytes += partitionMetrics.Txbytes
				}
				metricValues = append(metricValues, metricValue{[]string{topicName}, totalBytes})
			}
			return metricValues, nil
		}},
		dimension: clientIDTopicDim},
	{
		//Total number of messages transmitted (produced)
		name:  "go_producer_metrics_record_send_total",
		mType: io_prometheus_client.MetricType_GAUGE,
		converter: &functionMetricConverter{func(metrics stats) ([]metricValue, error) {
			return []metricValue{{[]string{}, metrics.TotalMsgTransm}}, nil
		}},
		dimension: clientIDDim},
	{
		//Total number of request retries
		name:  "go_producer_topic_metrics_record_retry_total",
		mType: io_prometheus_client.MetricType_GAUGE,
		converter: &functionMetricConverter{func(stats stats) ([]metricValue, error) {
			total := float64(0)
			for _, brokerMetrics := range stats.Brokers {
				total += brokerMetrics.TotalRetries
			}
			return []metricValue{{[]string{}, total}}, nil
		}},
		dimension: clientIDDim},
	{
		//Total number of transmission errors
		name:  "go_producer_topic_metrics_record_error_total",
		mType: io_prometheus_client.MetricType_GAUGE,
		converter: &functionMetricConverter{func(stats stats) ([]metricValue, error) {
			total := float64(0)
			for _, brokerMetrics := range stats.Brokers {
				total += brokerMetrics.TotalTransmErrors
			}
			return []metricValue{{[]string{}, total}}, nil
		}},
		dimension: clientIDDim},
	{
		//Total number of receive errors
		name:  "go_producer_topic_metrics_request_error_total",
		mType: io_prometheus_client.MetricType_GAUGE,
		converter: &functionMetricConverter{func(stats stats) ([]metricValue, error) {
			total := float64(0)
			for _, brokerMetrics := range stats.Brokers {
				total += brokerMetrics.TotalRqstErrors
			}
			return []metricValue{{[]string{}, total}}, nil
		}},
		dimension: clientIDDim},
	{
		//Total number of requests timed out
		name:  "go_producer_topic_metrics_request_timeout_total",
		mType: io_prometheus_client.MetricType_GAUGE,
		converter: &functionMetricConverter{func(stats stats) ([]metricValue, error) {
			total := float64(0)
			for _, brokerMetrics := range stats.Brokers {
				total += brokerMetrics.TotalRqstTimeouts
			}
			return []metricValue{{[]string{}, total}}, nil
		}},
		dimension: clientIDDim},
	{
		//Request Size in Bytes
		name:  "go_producer_metrics_request_size_avg",
		mType: io_prometheus_client.MetricType_GAUGE,
		converter: &functionMetricConverter{func(stats stats) ([]metricValue, error) {
			if stats.TotalMsgTransm == 0 {
				return []metricValue{{[]string{}, 0.0}}, nil
			}
			return []metricValue{{[]string{}, stats.TotalMsgBytesTransm / stats.TotalMsgTransm}}, nil
		}},
		dimension: clientIDDim},
	{
		//Broker latency / round-trip time in microseconds
		name:  "go_producer_metrics_request_latency_avg",
		mType: io_prometheus_client.MetricType_GAUGE,
		converter: &functionMetricConverter{func(stats stats) ([]metricValue, error) {
			total := float64(0)
			for _, brokerMetrics := range stats.Brokers {
				total += brokerMetrics.BrokerLatency.Avg
			}
			if len(stats.Brokers) == 0 {
				return []metricValue{{[]string{}, 0.0}}, nil
			}
			return []metricValue{{[]string{}, total / float64(len(stats.Brokers))}}, nil
		}},
		dimension: clientIDDim},
	{
		//Internal producer queue latency in microseconds
		name:  "go_producer_metrics_internal_request_latency_avg",
		mType: io_prometheus_client.MetricType_GAUGE,
		converter: &functionMetricConverter{func(stats stats) ([]metricValue, error) {
			total := float64(0)
			for _, brokerMetrics := range stats.Brokers {
				total += brokerMetrics.IntProducerLatency.Avg
			}
			if len(stats.Brokers) == 0 {
				return []metricValue{{[]string{}, 0.0}}, nil
			}
			return []metricValue{{[]string{}, total / float64(len(stats.Brokers))}}, nil
		}},
		dimension: clientIDDim},
	{
		//Number of connection attempts, including successful and failed, and name resolution failures.
		name:  "go_producer_metrics_connection_count_sum",
		mType: io_prometheus_client.MetricType_GAUGE,
		converter: &functionMetricConverter{func(stats stats) ([]metricValue, error) {
			total := float64(0)
			for _, brokerMetrics := range stats.Brokers {
				total += brokerMetrics.Connects
			}
			return []metricValue{{[]string{}, total}}, nil
		}},
		dimension: clientIDDim},
	{
		//Number of disconnects (triggered by broker, network, load-balancer, etc.).
		name:  "go_producer_metrics_disconnect_count_sum",
		mType: io_prometheus_client.MetricType_GAUGE,
		converter: &functionMetricConverter{func(stats stats) ([]metricValue, error) {
			total := float64(0)
			for _, brokerMetrics := range stats.Brokers {
				total += brokerMetrics.Disconnects
			}
			return []metricValue{{[]string{}, total}}, nil
		}},
		dimension: clientIDDim},
	{
		//Current number of messages in-flight to/from broker by topic
		name:  "go_producer_metrics_requests_in_flight",
		mType: io_prometheus_client.MetricType_GAUGE,
		converter: &functionMetricConverter{func(stats stats) ([]metricValue, error) {
			var metricValues []metricValue
			for topicName, topicMetrics := range stats.Topics {
				total := float64(0)
				for _, partitionMetrics := range topicMetrics.Partitions {
					total += partitionMetrics.MsgsInflight
				}
				metricValues = append(metricValues, metricValue{[]string{topicName}, total})
			}
			return metricValues, nil
		}},
		dimension: clientIDTopicDim},
	{
		//Record Size in bytes by topic
		name:  "go_producer_metrics_record_size_avg",
		mType: io_prometheus_client.MetricType_GAUGE,
		converter: &functionMetricConverter{func(stats stats) ([]metricValue, error) {
			var metricValues []metricValue
			for topicName, topicMetrics := range stats.Topics {
				total := float64(0)
				count := float64(0)
				for _, partitionMetrics := range topicMetrics.Partitions {
					if partitionMetrics.Txmsgs != 0 {
						total += partitionMetrics.Txbytes / partitionMetrics.Txmsgs
						count++
					}
				}
				if count != 0 {
					total /= count
				}
				metricValues = append(metricValues, metricValue{[]string{topicName}, total})
			}
			return metricValues, nil
		}},
		dimension: clientIDTopicDim},
	{
		//Broker throttling time in milliseconds
		name:  "go_producer_metrics_produce_throttle_time_avg",
		mType: io_prometheus_client.MetricType_GAUGE,
		converter: &functionMetricConverter{func(stats stats) ([]metricValue, error) {
			total := 0.0
			for _, brokerMetrics := range stats.Brokers {
				total += brokerMetrics.Throttle.Avg
			}
			if len(stats.Brokers) == 0 {
				return []metricValue{{[]string{}, 0.0}}, nil
			}
			return []metricValue{{[]string{}, total / float64(len(stats.Brokers))}}, nil
		}},
		dimension: clientIDDim},
	{
		//Total number of messages produced (possibly not yet transmitted) by topic
		name:  "go_producer_topic_metrics_record_send_total",
		mType: io_prometheus_client.MetricType_GAUGE,
		converter: &functionMetricConverter{func(metrics stats) ([]metricValue, error) {
			var metricValues []metricValue
			for topicName, topicMetrics := range metrics.Topics {
				sendTotal := float64(0)
				for _, partitionMetrics := range topicMetrics.Partitions {
					//This check is needed as librdkafka double counts some events when metadata for topic is not there
					//At startup, when metadata for topic is not there, message published go to the internal partition
					//But when partition info comes, messages are counted in the other partitions as well as this partition
					//This could probably be a design bug in librdkafka, we can put additional metric for this case
					if partitionMetrics.Partition == -1 {
						continue
					}
					sendTotal += partitionMetrics.Msgs
				}
				metricValues = append(metricValues, metricValue{[]string{topicName}, sendTotal})
			}
			return metricValues, nil
		}},
		dimension: clientIDTopicDim},
	{
		//Total number of messages produced by topic
		name:  "go_producer_topic_metrics_record_send_success_total",
		mType: io_prometheus_client.MetricType_GAUGE,
		converter: &functionMetricConverter{func(metrics stats) ([]metricValue, error) {
			var metricValues []metricValue
			for topicName, topicMetrics := range metrics.Topics {
				sendTotal := float64(0)
				for _, partitionMetrics := range topicMetrics.Partitions {
					sendTotal += partitionMetrics.Txmsgs
				}
				metricValues = append(metricValues, metricValue{[]string{topicName}, sendTotal})
			}
			return metricValues, nil
		}},
		dimension: clientIDTopicDim},
	{
		//Batch message counts by topic
		name:  "go_producer_metrics_batch_size",
		mType: io_prometheus_client.MetricType_GAUGE,
		converter: &functionMetricConverter{func(metrics stats) ([]metricValue, error) {
			var metricValues []metricValue
			for topicName, topicMetrics := range metrics.Topics {
				metricValues = append(metricValues, metricValue{[]string{topicName}, topicMetrics.BatchCnt.Avg})
			}
			return metricValues, nil
		}},
		dimension: clientIDTopicDim},
	{
		//Total number of requests sent to Kafka brokers
		name:  "go_producer_metrics_request_total",
		mType: io_prometheus_client.MetricType_GAUGE,
		converter: &functionMetricConverter{func(stats stats) ([]metricValue, error) {
			return []metricValue{{[]string{}, stats.TotalRqstSent}}, nil
		}},
		dimension: clientIDDim},
	{
		//Total number of responses received from Kafka brokers
		name:  "go_producer_metrics_response_total",
		mType: io_prometheus_client.MetricType_GAUGE,
		converter: &functionMetricConverter{func(stats stats) ([]metricValue, error) {
			return []metricValue{{[]string{}, stats.TotalRspRec}}, nil
		}},
		dimension: clientIDDim},
	{
		//Total number of bytes received from Kafka brokers
		name:  "go_producer_metrics_incoming_byte_total",
		mType: io_prometheus_client.MetricType_GAUGE,
		converter: &functionMetricConverter{func(stats stats) ([]metricValue, error) {
			return []metricValue{{[]string{}, stats.TotalRspBytesRec}}, nil
		}},
		dimension: clientIDDim},
	{
		//Total number of bytes transmitted to Kafka brokers
		name:  "go_producer_metrics_outgoing_byte_total",
		mType: io_prometheus_client.MetricType_GAUGE,
		converter: &functionMetricConverter{func(stats stats) ([]metricValue, error) {
			return []metricValue{{[]string{}, stats.TotalBytesSent}}, nil
		}},
		dimension: clientIDDim},
	{
		// Sum of Internal request queue latency in microseconds and Internal producer queue latency in milliseconds
		name:  "go_producer_metrics_record_queue_time_avg",
		mType: io_prometheus_client.MetricType_GAUGE,
		converter: &functionMetricConverter{func(stats stats) ([]metricValue, error) {
			total := float64(0)
			for _, brokerMetrics := range stats.Brokers {
				total += (brokerMetrics.IntProducerLatency.Avg + brokerMetrics.OutputBufLatency.Avg) / 1000 // As these values are in microseconds
			}
			if len(stats.Brokers) == 0 {
				return []metricValue{{[]string{}, 0.0}}, nil
			}
			return []metricValue{{[]string{}, total / float64(len(stats.Brokers))}}, nil
		}},
		dimension: clientIDDim},
	{
		name:  "go_kafka_lib_version_info",
		mType: io_prometheus_client.MetricType_COUNTER,
		converter: &functionMetricConverter{func(stats stats) ([]metricValue, error) {
			return []metricValue{{[]string{"", Version, ConfluentKafkaVersion}, 1.0}}, nil
		}},
		dimension: clientIDTopicLibDim},
	{
		name:  "go_confluent_kafka_version",
		mType: io_prometheus_client.MetricType_COUNTER,
		converter: &functionMetricConverter{func(stats stats) ([]metricValue, error) {
			_, kafkaVersionStr := kafka.LibraryVersion()
			return []metricValue{{[]string{"", kafkaVersionStr}, 1.0}}, nil
		}},
		dimension: clientIDKafkaVerDim},
}
