package swgykafka

import (
	"context"
	"errors"
	"fmt"
	"github.com/confluentinc/confluent-kafka-go/kafka"
	"os"
	"os/signal"
	"runtime/debug"
	"sync"
	"syscall"
	"time"
)

//region Kafka Consumer
type kafkaConsumer interface {
	commitMessage(m *kafka.Message) ([]kafka.TopicPartition, error)
	commitOffsets(offsets []kafka.TopicPartition) ([]kafka.TopicPartition, error)
	storeOffset(m *kafka.Message) ([]kafka.TopicPartition, error)
	subscribe(topic string, rebalanceCb kafka.RebalanceCb) error
	poll(timeoutMs int) (event kafka.Event)
	close() (err error)
	assignment() (partitions []kafka.TopicPartition, err error)
	offsetsForTimes(partition []kafka.TopicPartition, findOffsetForTimeTimeout int) (offsets []kafka.TopicPartition, err error)
	seek(offset kafka.TopicPartition, seekTimeout int) error
	unassign() error
	assign([]kafka.TopicPartition) error
}

type kafkaConsumerRebalanceListener interface {
	reBalanceCallback(consumer *kafka.Consumer, event kafka.Event) error
}

type concreteKafkaConsumer struct {
	*kafka.Consumer
}

func (consumer *concreteKafkaConsumer) commitMessage(m *kafka.Message) ([]kafka.TopicPartition, error) {
	return consumer.CommitMessage(m)
}

func (consumer *concreteKafkaConsumer) commitOffsets(offsets []kafka.TopicPartition) ([]kafka.TopicPartition, error) {
	return consumer.CommitOffsets(offsets)
}

func (consumer *concreteKafkaConsumer) storeOffset(m *kafka.Message) ([]kafka.TopicPartition, error) {
	toppar := []kafka.TopicPartition{m.TopicPartition}
	// This offset increment is done even in confluent-kafk-go lib in CommitMessage(m *Message) function
	toppar[0].Offset++
	return consumer.StoreOffsets(toppar)
}

func (consumer *concreteKafkaConsumer) subscribe(topic string, rebalanceCb kafka.RebalanceCb) error {
	return consumer.Subscribe(topic, rebalanceCb)
}

func (consumer *concreteKafkaConsumer) poll(timeoutMs int) kafka.Event {
	return consumer.Poll(timeoutMs)
}

func (consumer *concreteKafkaConsumer) close() error {
	return consumer.Close()
}

func (consumer *concreteKafkaConsumer) assignment() (partitions []kafka.TopicPartition, err error) {
	return consumer.Assignment()
}

func (consumer *concreteKafkaConsumer) seek(offset kafka.TopicPartition, seekTimeout int) error {
	return consumer.Seek(offset, seekTimeout)
}

func (consumer *concreteKafkaConsumer) offsetsForTimes(partition []kafka.TopicPartition, findOffsetForTimeTimeout int) (offsets []kafka.TopicPartition, err error) {
	return consumer.OffsetsForTimes(partition, findOffsetForTimeTimeout)
}

func (consumer *concreteKafkaConsumer) unassign() error {
	return consumer.Unassign()
}

func (consumer *concreteKafkaConsumer) assign(partitions []kafka.TopicPartition) error {
	return consumer.Assign(partitions)
}

//endregion

//region Consumer Thread
type ConsumerThread struct {
	name                     string
	config                   ConsumerConfig
	primary                  bool
	handler                  ConsumerHandler
	processAll               bool
	lastProcessedTimes       map[string]int64
	partitionSeekTimes       map[kafka.TopicPartition]int64
	forceProcessingEventChan chan forceProcessingEvent
	kConsumer                kafkaConsumer
	failureHandler           *failureHandler
	shutdownChan             chan interface{}
	metricChan               chan *consumerMetric
	shutDownWaitGroup        *sync.WaitGroup
	autoAssignPartitions     bool
	partitionsToRevoke       []kafka.TopicPartition
	partitionsToSubscribe    []kafka.TopicPartition
	rebalanceListener        kafkaConsumerRebalanceListener
	ctx                      context.Context
}

func NewConsumerThread(name string, config ConsumerConfig,
	primary bool,
	handler ConsumerHandler,
	retryProducer *Producer,
	retryTopic string,
	deadletterTopic string,
	shutdownChan chan interface{},
	shutDownWaitGroup *sync.WaitGroup,
	forceProcessingEventChan chan forceProcessingEvent,
	autoAssignPartitions bool,
	metricsChan chan *consumerMetric) (*ConsumerThread, error) {
	// initialize context
	ctx := NewContext(context.Background(),
		AutoField(isPrimaryLogKey, primary),
		AutoField("topic", config.topic.name),
		AutoField("consumer-group-id", config.consumerGroupID),
		AutoField("client-id", config.clientID),
		AutoField("consumer-thread-name", name))

	logger := WithContext(ctx)

	configMap, _ := config.toKafkaConfig(primary)
	c, err := kafka.NewConsumer(configMap)

	if err != nil {
		logger.Error("Failed to create consumer", ErrorField(err))
		return nil, err
	}

	if handler == nil {
		return nil, errors.New("handler is required")
	}

	fHandler := newFailureHandler(config.consumerGroupID, config.retryConfig, retryProducer, retryTopic, deadletterTopic, ctx)

	return &ConsumerThread{
		name:                     name,
		config:                   config,
		primary:                  primary,
		handler:                  handler,
		kConsumer:                &concreteKafkaConsumer{c},
		failureHandler:           fHandler,
		shutdownChan:             shutdownChan,
		shutDownWaitGroup:        shutDownWaitGroup,
		metricChan:               metricsChan,
		lastProcessedTimes:       make(map[string]int64),
		partitionSeekTimes:       make(map[kafka.TopicPartition]int64),
		forceProcessingEventChan: forceProcessingEventChan,
		autoAssignPartitions:     autoAssignPartitions,
		partitionsToRevoke:       make([]kafka.TopicPartition, 0),
		partitionsToSubscribe:    make([]kafka.TopicPartition, 0),
		ctx:                      ctx,
	}, nil
}

func (conTh *ConsumerThread) registerRebalanceListener(rebalanceListener kafkaConsumerRebalanceListener) {
	conTh.rebalanceListener = rebalanceListener
}

func (conTh *ConsumerThread) run() error {
	logger := WithContext(conTh.ctx)
	logger.Info("Created Consumer", AutoField("kConsumer", conTh.kConsumer))
	if conTh.autoAssignPartitions {
		err := conTh.kConsumer.subscribe(conTh.config.topic.name, conTh.reBalanceCallback)
		if err != nil {
			logger.Error("Not able to subscribe", ErrorField(err))
			return err
		}
		conTh.initLastProcessedTimes()
	}

	conTh.shutDownWaitGroup.Add(1)
	go conTh.start()
	return nil
}

func (conTh *ConsumerThread) reassignPartitionsIfAny() bool {
	logger := WithContext(conTh.ctx)

	// Handle manual partition assignment
	if conTh.autoAssignPartitions {
		return true
	}
	// Check if we have got partitions to revoke and unassign from the consumer
	if len(conTh.partitionsToRevoke) != 0 {
		logger.Info("Un-subscribing the partitions")
		err := conTh.kConsumer.unassign()
		if err == nil {
			conTh.partitionsToRevoke = make([]kafka.TopicPartition, 0)
		} else {
			logger.Error("Error Un-subscribing the partitions", ErrorField(err))
		}
	}
	// Check if we have got partitions to assign and subscribe to the consumer
	if len(conTh.partitionsToSubscribe) != 0 {
		logger.Info("Subscribing to the partitions", AutoField("partitions", conTh.partitionsToSubscribe))
		err := conTh.kConsumer.assign(conTh.partitionsToSubscribe)
		if err == nil {
			assigned, err := conTh.kConsumer.assignment()
			if err == nil && len(assigned) != 0 {
				conTh.removeFromSubscribedArray(assigned)
				conTh.partitionsToRevoke = make([]kafka.TopicPartition, 0)
			} else {
				logger.Error("Error getting assigned partitions", AutoField("partitions", conTh.partitionsToSubscribe), ErrorField(err))
				time.Sleep(1000)
				return false
			}
		} else {
			logger.Error("Error Subscribing to the partitions", AutoField("partitions", conTh.partitionsToSubscribe), ErrorField(err))
			time.Sleep(1000)
			return false
		}
	}
	return true
}

func (conTh *ConsumerThread) removeFromSubscribedArray(rArray []kafka.TopicPartition) {
	mb := make(map[int32]struct{}, len(rArray))
	for _, x := range rArray {
		mb[x.Partition] = struct{}{}
	}
	var diff []kafka.TopicPartition
	for _, x := range conTh.partitionsToSubscribe {
		if _, found := mb[x.Partition]; !found {
			diff = append(diff, x)
		}
	}
	conTh.partitionsToSubscribe = diff
}

func (conTh *ConsumerThread) initLastProcessedTimes() {
	logger := WithContext(conTh.ctx)

	if len(conTh.lastProcessedTimes) > 0 {
		return
	}

	asgnmnt, err := conTh.kConsumer.assignment()
	if err != nil {
		logger.Error("Not able to get assigned partitions", ErrorField(err))
	} else {
		for _, v := range asgnmnt {
			conTh.lastProcessedTimes[kafkaPartitionToString(v)] = -1
		}
	}
}

func (conTh *ConsumerThread) reBalanceCallback(consumer *kafka.Consumer, event kafka.Event) error {
	logger := WithContext(conTh.ctx)

	switch e := event.(type) {
	case kafka.AssignedPartitions:
		logger.Info("Assigned Partitions", AutoField("partitions", e))
		if !conTh.autoAssignPartitions {
			pSubscribe := make([]kafka.TopicPartition, 0)
			for _, tp := range e.Partitions {
				topicPartition := kafka.TopicPartition{
					Topic:     &conTh.config.topic.name,
					Partition: tp.Partition,
					//Offset should be set to offsetinvalid for looking up last position on broker as per edenhill
					//https://gitter.im/edenhill/librdkafka?at=5785520ab79455146fa80d0e
					Offset: kafka.OffsetInvalid,
				}
				pSubscribe = append(pSubscribe, topicPartition)
			}
			conTh.partitionsToSubscribe = pSubscribe
		} else {
			for _, tp := range e.Partitions {
				conTh.lastProcessedTimes[kafkaPartitionToString(tp)] = -1
			}
			if conTh.rebalanceListener != nil {
				conTh.rebalanceListener.reBalanceCallback(consumer, event)
			}
		}

	case kafka.RevokedPartitions:
		logger.Info("Revoked Partitions", AutoField("partitions", e))
		if !conTh.autoAssignPartitions {
			pRevoke := make([]kafka.TopicPartition, 0)
			for _, tp := range e.Partitions {
				topicPartition := kafka.TopicPartition{
					Topic:     &conTh.config.topic.name,
					Partition: tp.Partition,
					Offset:    kafka.OffsetInvalid,
				}
				pRevoke = append(pRevoke, topicPartition)
			}
			conTh.partitionsToRevoke = pRevoke
		} else {
			for _, tp := range e.Partitions {
				delete(conTh.lastProcessedTimes, kafkaPartitionToString(tp))
			}
			if conTh.rebalanceListener != nil {
				conTh.rebalanceListener.reBalanceCallback(consumer, event)
			}
		}
	}
	return nil
}

func kafkaPartitionToString(partition kafka.TopicPartition) string {
	return fmt.Sprintf("%v-%v", *partition.Topic, partition.Partition)
}

func (conTh *ConsumerThread) start() {
	logger := WithContext(conTh.ctx)

	defer conTh.shutDownWaitGroup.Done()
	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, syscall.SIGINT, syscall.SIGTERM)

	logger.Info("Starting the consumer thread")
	for {
		select {
		case sig := <-sigChan:
			logger.Info("Caught signal and terminating", AutoField("signal", sig))
			conTh.shutdown()
			return
		case <-conTh.shutdownChan:
			logger.Info("Received shutdown event and terminating")
			return
		case fpe := <-conTh.forceProcessingEventChan:
			logger.Info("Received force processing event", AutoField("from-time-stamp", fpe.fromTimeStamp),
				AutoField("fpe-event", fpe))
			conTh.processAll = fpe.processAll
			if fpe.fromTimeStamp > 0 {
				conTh.partitionSeekTimes[fpe.topicPartition] = fpe.fromTimeStamp
			}
		default:
			if !conTh.reassignPartitionsIfAny() {
				continue
			}
			conTh.resetOffset()
			ev := conTh.kConsumer.poll(conTh.config.pollTimeoutInMs)
			logger.Debug("Polled Broker")
			shouldShutdownThread := conTh.handleEvent(ev)
			if shouldShutdownThread {
				conTh.shutdown()
				return
			}
		}
	}
}

func (conTh *ConsumerThread) resetOffset() {
	logger := WithContext(conTh.ctx)

	if len(conTh.partitionSeekTimes) == 0 {
		return
	}

	assgnPartitions, err := conTh.kConsumer.assignment()
	if err != nil {
		logger.Error("error in getting assigned partitions", ErrorField(err))
		return
	}
	if len(assgnPartitions) == 0 {
		logger.Warn("no assigned partitions")
		return
	}

	logger.Info("partition offset to reset", AutoField("partitionSeekTimes", conTh.partitionSeekTimes), AutoField("assignedPartitions", assgnPartitions))
	for partition, offsetValue := range conTh.partitionSeekTimes {
		if containsTopic(assgnPartitions, partition) {
			if err := conTh.seekToTimestamp(partition, offsetValue, conTh.config.findOffsetForTimeTimeout, conTh.config.offsetSeekTimeout); err != nil {
				logger.Error("error in seeking while resetting offset", AutoField(partitionLogKey, partition.Partition), ErrorField(err))
			} else {
				logger.Info("partition offset reset", AutoField(partitionLogKey, partition.Partition), AutoField("offset", offsetValue))
				delete(conTh.partitionSeekTimes, partition)
			}
		} else {
			logger.Warn("partition offset seek for unassigned partition", AutoField(partitionLogKey, partition.Partition),
				AutoField("offset", offsetValue),
				AutoField("assignedPartitions", assgnPartitions))
		}
	}
}

func (conTh *ConsumerThread) seekToTimestamp(partition kafka.TopicPartition, resetTime int64, findOffsetForTimeTimeout int, seekTimeout int) error {
	logger := WithContext(conTh.ctx)

	partition.Offset = kafka.Offset(resetTime)
	offsets, err := conTh.kConsumer.offsetsForTimes([]kafka.TopicPartition{partition}, findOffsetForTimeTimeout)
	if err != nil {
		logger.Error("error seeking offset for topic", AutoField("offsets", offsets), AutoField("topic-partition", partition), ErrorField(err))
		return err
	}
	logger.Info("seeking offset for topic", AutoField("offsets", offsets), AutoField("topic-partition", partition))
	var seekErrors []error
	for _, offset := range offsets {
		if offset.Offset == -1 {
			logger.Info("Offset already at latest, no need to seek", AutoField(partitionLogKey, offset.Partition))
			continue
		}
		logger.Info("Seeking offset", AutoField(partitionLogKey, offset.Partition), AutoField("offset", offset.Offset))
		if err := conTh.kConsumer.seek(offset, seekTimeout); err != nil {
			seekErrors = append(seekErrors, err)
		} else {
			commitPartitons := append(make([]kafka.TopicPartition, 0), offset)
			part, err := conTh.kConsumer.commitOffsets(commitPartitons)
			logger.Info("committing partition offset after seeking", AutoField("responsePartition", part), AutoField("requestPartition", commitPartitons))
			if err != nil {
				seekErrors = append(seekErrors, err)
				logger.Error("Committing offsets failed after partitions seek", ErrorField(err), AutoField("requestPartition", commitPartitons))
			}
		}
	}
	return combineErrors(seekErrors)
}

func containsTopic(topics []kafka.TopicPartition, partition kafka.TopicPartition) bool {
	for _, topicP := range topics {
		if *topicP.Topic == *partition.Topic && topicP.Partition == partition.Partition {
			return true
		}
	}
	return false
}

func (conTh *ConsumerThread) shouldHandle(ev *kafka.Message) bool {
	var recordPathHeader *kafka.Header
	for _, h := range ev.Headers {
		if h.Key == HeaderRecordPath {
			//This is needed because everything in go is pass by value, if we copy h's address in another value and keep iterating
			//the value of h will keep changing and would point to the last iterated value. This is not what we intend to do.
			//Sample : https://play.golang.org/p/4cZKKp7HQmX
			temp := h
			recordPathHeader = &temp
		}
	}
	return conTh.processAll || recordPathHeader == nil || RecordPathPrimary == string(recordPathHeader.Value)
}

func (conTh *ConsumerThread) handleEvent(ev kafka.Event) (shouldShutdownThread bool) {
	if ev == nil {
		return false
	}

	logger := WithContext(conTh.ctx)

	defer func() {
		if r := recover(); r != nil {
			logger.Error("panic in handleEvent", AutoField("recover", r), AutoField("stack", string(debug.Stack())))
		}
	}()

	switch e := ev.(type) {
	case *kafka.Message:
		var err error
		record := toRecord(e)
		if conTh.shouldHandle(ev.(*kafka.Message)) {
			err = conTh.doHandle(record)
		}

		if err != nil {
			logger.Info("error while processing the record", ErrorField(err))
		} else {
			logger.Debug("message has been processed successfully")
		}

		if conTh.config.enableAutoCommit {
			conTh.storeOffset(e)
		} else {
			conTh.commitMsg(e)
		}

		conTh.lastProcessedTimes[kafkaPartitionToString(e.TopicPartition)] = toMillisUTC(e.Timestamp)
	case kafka.Error:
		logger.Error("error while handling event ", ErrorField(e))
		if e.IsFatal() {
			logger.Error("fatal error while handling event ", ErrorField(e))
			return true
		}
	case *kafka.Stats:
		var clusterId string
		if conTh.primary {
			clusterId = conTh.config.CommonConfig.primary.GetClusterId()
		} else {
			clusterId = conTh.config.CommonConfig.secondary.GetClusterId()
		}
		conTh.metricChan <- &consumerMetric{clientID: conTh.config.clientID, clusterID: clusterId, consumerTopic: conTh.config.topic.name, stats: e}
	case kafka.PartitionEOF:
		logger.Info("consumer reached end of partition ", AutoField("event", e.String()))
	case kafka.OffsetsCommitted:
		logger.Info("offset Committed ", AutoField("event", e.String()))
	default:
		logger.Info("got Unknown Event ", AutoField("event", ev.String()))
	}

	return false
}

func (conTh *ConsumerThread) doHandle(record *Record) error {
	logger := WithContext(conTh.ctx)

	var err error
	if conTh.config.delayInMs > 0 {
		err = conTh.waitBeforeProcessing(record)
	}
	if err != nil {
		logger.Info("not processing the record", AutoField("record", record), ErrorField(err))
		return err
	}

	status, err := conTh.handler.Handle(record)

	if err != nil {
		logger.Error("error while processing the record", AutoField("record", record), ErrorField(err))
		status = HardFailure
	}

	switch status {
	case SoftFailure:
		err = conTh.failureHandler.Handle(record, true)
	case HardFailure:
		err = conTh.failureHandler.Handle(record, false)
	case Success:
		err = nil
	default:
		logger.Info("got unknown status for record", AutoField("status", status), AutoField("record", record))
		err = fmt.Errorf("unknown status %v", status)
	}

	return err
}

func (conTh *ConsumerThread) waitBeforeProcessing(record *Record) error {
	sleepTime := toMillisUTC(record.Timestamp) + int64(conTh.config.delayInMs) - toMillisUTC(time.Now())
	if sleepTime <= 0 {
		return nil
	}
	if record.isRetryMsg() && record.retryConsumerGroup() != conTh.config.consumerGroupID { //FIXME this is being duplicated in retry handler. Find a cleaner way of doing this
		return nil
	}

	stepDuration := 1 * time.Second
	ticker := time.NewTicker(stepDuration)

	for sleepTime > 0 {
		select {
		case <-conTh.shutdownChan:
			return errors.New(fmt.Sprintf("shutdown called in thread - %v.  aborting", conTh.name))
		case <-ticker.C:
			sleepTime -= stepDuration.Milliseconds()
		}
	}
	ticker.Stop()
	return nil

}

func (conTh *ConsumerThread) commitMsg(msg *kafka.Message) {
	logger := WithContext(conTh.ctx)

	_, err := conTh.kConsumer.commitMessage(msg)
	if err != nil {
		logger.Info("commit failed for record", AutoField("message", msg), ErrorField(err))
	}
}

func (conTh *ConsumerThread) storeOffset(msg *kafka.Message) {
	logger := WithContext(conTh.ctx)

	_, err := conTh.kConsumer.storeOffset(msg)
	if err != nil {
		logger.Error("store Offset Failed for record", AutoField("message", msg), ErrorField(err))
	}
}

func (conTh *ConsumerThread) shutdown() {
	logger := WithContext(conTh.ctx)
	logger.Info("shutting down Consumer Thread")
	err := conTh.kConsumer.close()
	if err != nil {
		logger.Error("error occurred while closing consumer", ErrorField(err))
	}
}

//endregion
