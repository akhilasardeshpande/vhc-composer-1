package swgykafka

import (
	"sync"
)

/**
 * Created by Sai Ravi Teja K on 06, Mar 2020
 * Source code is copied from http://marcio.io/2015/07/handling-1-million-requests-per-minute-with-golang/
 */

type dispatcher struct {
	// A pool of workers channels that are registered with the dispatcher
	workerPool chan chan Job
	maxWorkers int
	// A buffered channel that we can send work requests on.
	jobQueue    chan Job
	maxQueueLen int

	// Channel to shutdown all the workers
	shutdownChan                chan interface{}
	shutdownWaitGroup           *sync.WaitGroup
	shutdownOnce                sync.Once
}

func newDispatcher(maxWorkers int, maxQueueLen int) *dispatcher {
	if maxWorkers <= 0 || maxQueueLen <= 0 {
		return nil
	}

	pool := make(chan chan Job, maxWorkers)
	jobQueue := make(chan Job, maxQueueLen)
	showdownChan := make(chan interface{})
	return &dispatcher{workerPool: pool, jobQueue: jobQueue, shutdownChan: showdownChan, shutdownWaitGroup: &sync.WaitGroup{}, maxWorkers: maxWorkers, maxQueueLen: maxQueueLen}
}

func (d *dispatcher) run() {
	// starting n number of workers
	for i := 0; i < d.maxWorkers; i++ {
		worker := newWorker(d.workerPool, d.shutdownChan, d.shutdownWaitGroup)
		worker.start()
	}

	go d.dispatch()
}

func (d *dispatcher) dispatch() {
	for {
		select {
		// try to obtain a worker job channel that is available.
		// this will block until a worker is idle
		case jobChannel, ok := <-d.workerPool:
			if !ok {
				return
			}

			// a job request has been received
			job, ok := <-d.jobQueue
			if !ok {
				return
			}

			// dispatch the job to the worker job channel
			jobChannel <- job
		case <-d.shutdownChan:
			return
		}
	}
}

// Stop signals the worker to stop listening for work requests.
func (d *dispatcher) shutdown() {
	d.shutdownOnce.Do(func() {
		close(d.shutdownChan)
		d.shutdownWaitGroup.Wait()
	})
}

// Job represents the job to be run
type Job struct {
	process    func()
}

// worker represents the worker that executes the job
type worker struct {
	workerPool        chan chan Job
	jobChannel        chan Job
	shutdownChan      chan interface{}
	shutdownWaitGroup *sync.WaitGroup
}

func newWorker(workerPool chan chan Job, shutdownChan chan interface{}, wg *sync.WaitGroup) worker {
	return worker{
		workerPool:        workerPool,
		jobChannel:        make(chan Job),
		shutdownChan:      shutdownChan,
		shutdownWaitGroup: wg,
	}
}

// start method starts the run loop for the worker, listening for a shutdownChan channel in
// case we need to stop it
func (w worker) start() {
	w.shutdownWaitGroup.Add(1)
	go func() {
		for {
			// register the current worker into the worker queue.
			w.workerPool <- w.jobChannel

			select {
			case job, ok := <-w.jobChannel:
				if !ok {
					w.shutdownWaitGroup.Done()
					return
				}

				job.process()
			case <-w.shutdownChan:
				// we have received a signal to stop
				w.shutdownWaitGroup.Done()
				return
			}
		}
	}()
}