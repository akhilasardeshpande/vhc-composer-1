package swgykafka

import "context"

/**
 * Created by Sai Ravi Teja K on 18, Dec 2019
 * © Bundl Technologies Private Ltd.
 */

type clusterState struct {
	isHealthy            bool
	healthyCounter       int
	unhealthyCounter     int
	degradationThreshold int
}

func (cs *clusterState) update(up bool) {
	cs.isHealthy = cs.isClusterHealthyWithDegradationThreshold(up)
}

func (cs *clusterState) isClusterHealthyWithDegradationThreshold(up bool) bool {
	isHealthyWithDegradation := cs.isHealthy
	if isHealthyWithDegradation && !up {
		cs.unhealthyCounter = min(cs.unhealthyCounter+1, cs.degradationThreshold)
		return cs.unhealthyCounter < cs.degradationThreshold
	} else if !isHealthyWithDegradation && up {
		cs.healthyCounter = min(cs.healthyCounter+1, cs.degradationThreshold)
		return cs.healthyCounter >= cs.degradationThreshold
	}
	cs.healthyCounter = 0
	cs.unhealthyCounter = 0
	return isHealthyWithDegradation
}

func getClusterState(health *clusterHealth, degradationThreshold int) *clusterState {
	return &clusterState{
		isHealthy:            health.isHealthy,
		degradationThreshold: degradationThreshold,
	}
}

//region Fault Detector
type faultDetector struct {
	primary                 *Cluster
	secondary               *Cluster
	faultListener           FaultListener
	primaryClusterState     *clusterState
	secondaryClusterState   *clusterState
	primaryActive           bool
	secondaryActive         bool
	healthCheckIntervalInMs int
	degradationThreshold    int
	ctx                     context.Context
}

func newFaultDetector(primary *Cluster, secondary *Cluster, faultListener FaultListener, healthCheckIntervalInMs, degradationThreshold int) *faultDetector {
	ctx := NewContext(context.Background())

	return &faultDetector{
		primary:                 primary,
		secondary:               secondary,
		faultListener:           faultListener,
		primaryActive:           true,
		secondaryActive:         false,
		healthCheckIntervalInMs: healthCheckIntervalInMs,
		ctx:                     ctx,
		degradationThreshold:    degradationThreshold,
	}
}

func (fd *faultDetector) start() {
	// register primary cluster for health check
	healthCheckUpdatePrimary := healthCheckerUpdate{
		cluster:                 fd.primary,
		listeners:               map[healthListener]struct{}{fd: {}},
		healthCheckIntervalInMs: fd.healthCheckIntervalInMs,
		register:                true,
	}
	healthCheckerInstance.healthCheckerUpdateChan <- &healthCheckUpdatePrimary

	// register secondary cluster for health check
	healthCheckUpdateSecondary := healthCheckerUpdate{
		cluster:                 fd.secondary,
		listeners:               map[healthListener]struct{}{fd: {}},
		healthCheckIntervalInMs: fd.healthCheckIntervalInMs,
		register:                true,
	}
	healthCheckerInstance.healthCheckerUpdateChan <- &healthCheckUpdateSecondary
}

func (fd *faultDetector) stop() {
	// de-register primary cluster for health check
	healthCheckUpdatePrimary := healthCheckerUpdate{
		cluster:  fd.primary,
		register: false,
	}
	healthCheckerInstance.healthCheckerUpdateChan <- &healthCheckUpdatePrimary

	// de-register secondary cluster for health check
	healthCheckUpdateSecondary := healthCheckerUpdate{
		cluster:  fd.secondary,
		register: false,
	}
	healthCheckerInstance.healthCheckerUpdateChan <- &healthCheckUpdateSecondary
}

func (fd *faultDetector) onUpdate(cluster *Cluster, health *clusterHealth) {
	changed := false

	// check if primary health changed
	if fd.primary.equals(cluster) {
		if fd.primaryClusterState == nil {
			fd.primaryClusterState = getClusterState(health, fd.degradationThreshold)
			changed = true
		} else {
			wasClusterStateHealthy := fd.primaryClusterState.isHealthy
			fd.primaryClusterState.update(health.isHealthy)
			changed = fd.hasChanged(wasClusterStateHealthy, fd.primaryClusterState.isHealthy)
		}
	}

	// check if secondary health changed
	if fd.secondary.equals(cluster) {
		if fd.secondaryClusterState == nil {
			fd.secondaryClusterState = getClusterState(health, fd.degradationThreshold)
			changed = true
		} else {
			wasClusterStateHealthy := fd.secondaryClusterState.isHealthy
			fd.secondaryClusterState.update(health.isHealthy)
			changed = fd.hasChanged(wasClusterStateHealthy, fd.secondaryClusterState.isHealthy)
		}
	}

	// invoke detect if health status has changed for any cluster
	if changed {
		fd.detect()
	}
}

/**
 * Detects the failure and either failover to secondary or failback to primary. Below are the possible scenarios.
 *
 * #	p 	s 	p.active?	change		action
 * 1	0	0	0				p		failback
 * 2	0	0	0				s		do nothing
 * 3	0	0	1				p		do nothing
 * 4	0	0	1				s		failover
 * 5	0	1	0				p		failback
 * 6	0	1	0				s		do nothing
 * 7	1	0	1				p		do nothing
 * 8	1	0	1				s		do nothing
 * 9	1	1	1				p		failover
 * 10	1	1	1				s		do nothing
 */
func (fd *faultDetector) detect() {
	logger := WithContext(fd.ctx)

	logger.Info("fault detection triggered")
	primaryUp := fd.primaryClusterState == nil || fd.primaryClusterState.isHealthy
	secondaryUp := fd.secondaryClusterState == nil || fd.secondaryClusterState.isHealthy
	if fd.faultListener == nil {
		return
	}

	if primaryUp && !fd.primaryActive {
		fd.primaryActive = true
		fd.secondaryActive = false
		fd.faultListener.failback()
	} else if !primaryUp && secondaryUp && !fd.secondaryActive {
		fd.primaryActive = false
		fd.secondaryActive = true
		fd.faultListener.failover()
		logger.Info("failing over completed")
	}

	logger.Info("fault detected", AutoField("isPrimaryUp", primaryUp), AutoField("isSecondaryUp", secondaryUp))
}

func (fd *faultDetector) hasChanged(wasClusterHealthy, isClusterHealthy bool) bool {
	return isClusterHealthy != wasClusterHealthy
}

func (fd *faultDetector) isHealthy(primary bool) bool {
	if primary {
		return fd.primaryClusterState == nil || fd.primaryClusterState.isHealthy
	} else {
		return fd.secondaryClusterState == nil || fd.secondaryClusterState.isHealthy
	}
}

//endregion

//region Fault Listener Interface
type FaultListener interface {
	failover()
	failback()
}

//endregion
