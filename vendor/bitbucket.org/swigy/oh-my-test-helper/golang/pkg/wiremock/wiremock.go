package wiremock

import (
	"context"
	"fmt"
	"github.com/testcontainers/testcontainers-go"
	"github.com/testcontainers/testcontainers-go/wait"
	"os"
)

type WireMock struct {
	host          string
	port          string
	container     testcontainers.Container
	adminEndpoint string
}

func (w *WireMock) Host() string {
	return w.host
}

func (w *WireMock) Port() string {
	return w.port
}

func (w *WireMock) Stop() error {
	return w.container.Terminate(context.Background())
}

func NewContainer() (*WireMock, error) {
	os.Setenv("TC_HOST", "localhost")
	ctx := context.Background()
	req := testcontainers.ContainerRequest{
		Image:        "rodolpheche/wiremock",
		ExposedPorts: []string{"8080/tcp", "8443/tcp"},
		WaitingFor:   wait.ForListeningPort("8080"),
	}

	container, err := testcontainers.GenericContainer(ctx, testcontainers.GenericContainerRequest{
		ContainerRequest: req,
		Started:          true,
	})

	if err != nil {
		return nil, err
	}

	host, _ := container.Host(context.Background())
	port, _ := container.MappedPort(context.Background(), "8080")

	return &WireMock{
		host:          host,
		port:          port.Port(),
		container:     container,
		adminEndpoint: fmt.Sprintf("http://%s:%s", host, port.Port()),
	}, nil
}
