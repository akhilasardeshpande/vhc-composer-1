package kafka

import (
	"context"
	confluent "github.com/confluentinc/confluent-kafka-go/kafka"
	"time"
)

func (k *Kafka) CreateTopics(topics []string) error {
	var ts []confluent.TopicSpecification
	for _, topic := range topics {
		ts = append(ts, confluent.TopicSpecification{
			Topic:             topic,
			NumPartitions:     1,
			ReplicationFactor: 1,
		})
	}
	_, err := k.adminClient.CreateTopics(context.Background(), ts, confluent.SetAdminOperationTimeout(time.Second*60))
	return err
}

func (k *Kafka) Produce(message Message) error {
	deliveryChan := make(chan confluent.Event)
	var headers []confluent.Header
	for key, value := range message.Headers {
		headers = append(headers, confluent.Header{Key: key, Value: []byte(value)})
	}
	err := k.producer.Produce(&confluent.Message{
		TopicPartition: confluent.TopicPartition{Topic: &message.Topic, Partition: confluent.PartitionAny},
		Key:            []byte(message.Key),
		Value:          []byte(message.Value),
		Headers:        headers,
	}, deliveryChan)
	if err != nil {
		return err
	}
	e := <-deliveryChan
	m := e.(*confluent.Message)
	return m.TopicPartition.Error
}

func (k *Kafka) NewConsumer(topic string, openTime time.Duration) (*Consumer, error) {
	kafkaConsumer, err := confluent.NewConsumer(&confluent.ConfigMap{
		"bootstrap.servers":               k.bootstrapServers,
		"broker.address.family":           "v4",
		"group.id":                        "oh_my_test_helper",
		"session.timeout.ms":              6000,
		"auto.offset.reset":               "earliest",
		"go.events.channel.enable":        true,
		"go.application.rebalance.enable": true,
	})
	if err != nil {
		return nil, err
	}
	err = kafkaConsumer.SubscribeTopics([]string{topic}, nil)
	outputChan := make(chan Message)
	consumer := &Consumer{msgChan: outputChan, kafkaConsumer: kafkaConsumer}

	go func() {
		for {
			select {
			case ev := <-kafkaConsumer.Events():
				switch e := ev.(type) {
				case confluent.AssignedPartitions:
					kafkaConsumer.Assign(e.Partitions)
				case confluent.RevokedPartitions:
					kafkaConsumer.Unassign()
				case *confluent.Message:
					headers := map[string]string{}
					for _, h := range e.Headers {
						headers[h.Key] = string(h.Value)
					}
					outputChan <- Message{
						Topic:   *e.TopicPartition.Topic,
						Key:     string(e.Key),
						Value:   string(e.Value),
						Headers: headers,
					}
				}
			case <-time.Tick(openTime):
				consumer.Close()
				break
			}
		}
	}()
	return consumer, nil
}

func (k *Kafka) ClearTopics(topics []string) error {
	if _, err := k.adminClient.DeleteTopics(context.Background(), topics); err != nil {
		return err
	}
	return k.CreateTopics(topics)
}
func (k *Kafka) isPresent(message Message) bool {
	// return the matching messages are found. Or return false if message is not found the topic.
	return true
}

func (k *Kafka) isDeadLettered(message Message) bool {
	// return the matching messages are found in dead letter topic . Or return false if message is not found the deadletter topic.
	return true
}
