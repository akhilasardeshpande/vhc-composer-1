// Code generated by github.com/99designs/gqlgen, DO NOT EDIT.

package model

import (
	"fmt"
	"io"
	"strconv"

	"github.com/99designs/gqlgen/graphql"
)

type Attachment struct {
	ID            int64   `json:"id"`
	ContentType   *string `json:"content_type"`
	Size          *int64  `json:"size"`
	Name          *string `json:"name"`
	AttachmentURL *string `json:"attachment_url"`
	CreatedAt     *string `json:"created_at"`
	UpdatedAt     *string `json:"updated_at"`
}

type BusinessMetrics struct {
	TotalOrdersCurrent                 int64   `json:"totalOrdersCurrent"`
	TotalOrdersPrevious                int64   `json:"totalOrdersPrevious"`
	TotalOrdersArrow                   string  `json:"totalOrdersArrow"`
	TotalRevenueCurrent                float64 `json:"totalRevenueCurrent"`
	TotalRevenuePrevious               float64 `json:"totalRevenuePrevious"`
	TotalRevenueArrow                  string  `json:"totalRevenueArrow"`
	DeliveredOrdersCurrent             int64   `json:"deliveredOrdersCurrent"`
	DeliveredOrdersPrevious            int64   `json:"deliveredOrdersPrevious"`
	DeliveredOrdersArrow               string  `json:"deliveredOrdersArrow"`
	DeliveredRevenueCurrent            float64 `json:"deliveredRevenueCurrent"`
	DeliveredRevenuePrevious           float64 `json:"deliveredRevenuePrevious"`
	DeliveredRevenueArrow              string  `json:"deliveredRevenueArrow"`
	CancelledOrdersCurrent             int64   `json:"cancelledOrdersCurrent"`
	CancelledOrdersPrevious            int64   `json:"cancelledOrdersPrevious"`
	CancelledOrdersArrow               string  `json:"cancelledOrdersArrow"`
	CancelledRevenueCurrent            float64 `json:"cancelledRevenueCurrent"`
	CancelledRevenuePrevious           float64 `json:"cancelledRevenuePrevious"`
	CancelledRevenueArrow              string  `json:"cancelledRevenueArrow"`
	RestaurantCancelledOrdersCurrent   int64   `json:"restaurantCancelledOrdersCurrent"`
	RestaurantCancelledOrdersPrevious  int64   `json:"restaurantCancelledOrdersPrevious"`
	RestaurantCancelledOrdersArrow     string  `json:"restaurantCancelledOrdersArrow"`
	RestaurantCancelledRevenueCurrent  float64 `json:"restaurantCancelledRevenueCurrent"`
	RestaurantCancelledRevenuePrevious float64 `json:"restaurantCancelledRevenuePrevious"`
	RestaurantCancelledRevenueArrow    string  `json:"restaurantCancelledRevenueArrow"`
	ReconPendingOrdersCurrent          int64   `json:"reconPendingOrdersCurrent"`
	ReconPendingOrdersPrevious         int64   `json:"reconPendingOrdersPrevious"`
	ReconPendingOrdersArrow            string  `json:"reconPendingOrdersArrow"`
	ReconPendingRevenueCurrent         float64 `json:"reconPendingRevenueCurrent"`
	ReconPendingRevenuePrevious        float64 `json:"reconPendingRevenuePrevious"`
	ReconPendingRevenueArrow           string  `json:"reconPendingRevenueArrow"`
	AverageOrderValueCurrent           float64 `json:"averageOrderValueCurrent"`
	AverageOrderValuePrevious          float64 `json:"averageOrderValuePrevious"`
	AverageOrderValueArrow             string  `json:"averageOrderValueArrow"`
}

type CartSessions struct {
	Period   string  `json:"period"`
	Previous int64   `json:"previous"`
	Current  int64   `json:"current"`
	Vendor   *Vendor `json:"vendor"`
}

type Conversation struct {
	BodyText     *string       `json:"body_text"`
	Body         *string       `json:"body"`
	ID           string        `json:"id"`
	Incoming     *bool         `json:"incoming"`
	Private      *bool         `json:"private"`
	UserID       *int64        `json:"user_id"`
	SupportEmail *string       `json:"support_email"`
	Source       *int64        `json:"source"`
	TicketID     *int64        `json:"ticket_id"`
	CreatedAt    *string       `json:"created_at"`
	UpdatedAt    *string       `json:"updated_at"`
	FromEmail    *string       `json:"from_email"`
	ToEmails     []*string     `json:"to_emails"`
	CcEmails     []*string     `json:"cc_emails"`
	BccEmails    []*string     `json:"bcc_emails"`
	Attachments  []*Attachment `json:"attachments"`
	Category     *int64        `json:"category"`
}

type ConversionMetrics struct {
	MenuSession     int64       `json:"menuSession"`
	CartSession     int64       `json:"cartSession"`
	Orders          int64       `json:"orders"`
	CartConversion  interface{} `json:"cartConversion"`
	OrderConversion interface{} `json:"orderConversion"`
}

type ConversionMetricsResponse struct {
	Period               string             `json:"period"`
	Vendor               *Vendor            `json:"vendor"`
	Current              *ConversionMetrics `json:"current"`
	Past                 *ConversionMetrics `json:"past"`
	CartConversionArrow  string             `json:"cartConversionArrow"`
	OrderConversionArrow string             `json:"orderConversionArrow"`
}

type CreateTicketInput struct {
	Name             *string           `json:"name"`
	RequesterID      *int64            `json:"requester_id"`
	Email            *string           `json:"email"`
	FacebookID       *string           `json:"facebook_id"`
	Phone            *string           `json:"phone"`
	TwitterID        *string           `json:"twitter_id"`
	UniqueExternalID *string           `json:"unique_external_id"`
	Subject          *string           `json:"subject"`
	Type             string            `json:"type"`
	Status           *int64            `json:"status"`
	Priority         *int64            `json:"priority"`
	Description      *string           `json:"description"`
	ResponderID      *int64            `json:"responder_id"`
	Attachments      []*graphql.Upload `json:"attachments"`
	CcEmails         []*string         `json:"cc_emails"`
	CustomFields     *CustomFieldInput `json:"custom_fields"`
	DueBy            *string           `json:"due_by"`
	EmailConfigID    *int64            `json:"email_config_id"`
	FrDueBy          *string           `json:"fr_due_by"`
	GroupID          *int64            `json:"group_id"`
	ProductID        *int64            `json:"product_id"`
	Source           *int64            `json:"source"`
	Tags             []*string         `json:"tags"`
	CompanyID        *int64            `json:"company_id"`
	InternalAgentID  *int64            `json:"internal_agent_id"`
	InternalGroupID  *int64            `json:"internal_group_id"`
}

type CustomField struct {
	NumberOfMenusUpdated                                                   *string `json:"number_of_menus_updated"`
	Reason1                                                                *string `json:"reason1"`
	NumberOfOutlets                                                        *string `json:"number_of_outlets"`
	TypeOfTask                                                             *string `json:"type_of_task"`
	CfExternalPos                                                          *string `json:"cf_external_pos"`
	Cf24HoursWaitingOnCustomer                                             *bool   `json:"cf_24_hours_waiting_on_customer"`
	CfLevel1Disposition                                                    *string `json:"cf_level_1_disposition"`
	CfLevel2Disposition                                                    *string `json:"cf_level_2_disposition"`
	CfLevel3Disposition                                                    *string `json:"cf_level_3_disposition"`
	CfCheckCaseApplicable                                                  *string `json:"cf_check_case_applicable"`
	CfNextSteps                                                            *string `json:"cf_next_steps"`
	CfCustomization                                                        *string `json:"cf_customization"`
	CfDigitizedBy                                                          *string `json:"cf_digitized_by"`
	CfRestaurantPhoneNumber                                                *int64  `json:"cf_restaurant_phone_number"`
	CfDigitizationTimeStamp                                                *string `json:"cf_digitization_time_stamp"`
	CfCatalogResolvedTimeStamp                                             *string `json:"cf_catalog_resolved_time_stamp"`
	CfCatalogStartTime                                                     *string `json:"cf_catalog_start_time"`
	CfRestaurantID                                                         *int64  `json:"cf_restaurant_id"`
	CfCityType                                                             *string `json:"cf_city_type"`
	CfAsmMobile                                                            *string `json:"cf_asm_mobile"`
	CfFssaiLicenceValidity                                                 *string `json:"cf_fssai_licence_validity"`
	CfArea                                                                 *string `json:"cf_area"`
	CfNoOfItemsDiscrepancy                                                 *string `json:"cf_no_of_items_discrepancy"`
	CfNoOfAttemptsMadeForAutomation                                        *string `json:"cf_no_of_attempts_made_for_automation"`
	CfAgentAssignmentTime                                                  *string `json:"cf_agent_assignment_time"`
	CfChildRidForPhotoReplication                                          *string `json:"cf_child_rid_for_photo_replication"`
	CfRestaurantOwnerName                                                  *string `json:"cf_restaurant_owner_name"`
	CfRestaurantOwnerPhoneNumber1                                          *string `json:"cf_restaurant_owner_phone_number_1"`
	CfRestaurantManagerName                                                *string `json:"cf_restaurant_manager_name"`
	CfRestaurantManagerPhoneNumber1                                        *string `json:"cf_restaurant_manager_phone_number_1"`
	CfTicketCreationSource                                                 *string `json:"cf_ticket_creation_source"`
	CfRestaurantNotRespondingSeenInOmsLogs                                 *string `json:"cf_restaurant_not_responding_seen_in_oms_logs"`
	CfItemOutOfStockSeenInOmsLogs                                          *string `json:"cf_item_out_of_stock_seen_in_oms_logs"`
	CfRestaurantClosureSeenInOmsLogs                                       *string `json:"cf_restaurant_closure_seen_in_oms_logs"`
	CfDeIDForOrder                                                         *string `json:"cf_de_id_for_order"`
	CfIsSelect                                                             *bool   `json:"cf_is_select"`
	CfTier                                                                 *string `json:"cf_tier"`
	CfNoOfItemsCreated                                                     *string `json:"cf_no_of_items_created"`
	CfOrderID                                                              *string `json:"cf_order_id"`
	CfRestID                                                               *int64  `json:"cf_rest_id"`
	CfGstEnabled                                                           *bool   `json:"cf_gst_enabled"`
	CfOrdersNotifyMobile                                                   *string `json:"cf_orders_notify_mobile"`
	CfNoOfItemsRequestedForCreation                                        *string `json:"cf_no_of_items_requested_for_creation"`
	CfRevisionsAgent                                                       *string `json:"cf_revisions_agent"`
	CfSmName                                                               *string `json:"cf_sm_name"`
	CfPopulateAllFields                                                    *bool   `json:"cf_populate_all_fields"`
	CfSmEmail                                                              *string `json:"cf_sm_email"`
	CfAdditionalRidForWhichSameResolutionIsToBeDone                        *string `json:"cf_additional_rid_for_which_same_resolution_is_to_be_done"`
	CfFssaiDocumentURL                                                     *string `json:"cf_fssai_document_url"`
	CfAsmName                                                              *string `json:"cf_asm_name"`
	RestaurantName                                                         *string `json:"restaurant_name"`
	CfFssaiLicenceNumber                                                   *string `json:"cf_fssai_licence_number"`
	CfAsmEmail                                                             *string `json:"cf_asm_email"`
	CfFullInformationTimeEnd                                               *string `json:"cf_full_information_time_end"`
	CfTypeOfPartner                                                        *int64  `json:"cf_type_of_partner"`
	CfFullInformationTimeStart                                             *string `json:"cf_full_information_time_start"`
	CfRestaurantName                                                       *string `json:"cf_restaurant_name"`
	RtpinternalUseOnly                                                     *string `json:"rtpinternal_use_only"`
	CfAreaCode                                                             *int64  `json:"cf_area_code"`
	CfMultipleRid                                                          *string `json:"cf_multiple_rid"`
	SalesPoc                                                               *string `json:"sales_poc"`
	CfReopenReason                                                         *string `json:"cf_reopen_reason"`
	CfRestaurantCity                                                       *string `json:"cf_restaurant_city"`
	VMPoc                                                                  *string `json:"vm_poc"`
	CfExecutedBy                                                           *string `json:"cf_executed_by"`
	CfCityCode                                                             *int64  `json:"cf_city_code"`
	UnregisteredUserCity                                                   *string `json:"unregistered_user_city"`
	CfSmMobile                                                             *string `json:"cf_sm_mobile"`
	CfFreezeTime                                                           *string `json:"cf_freeze_time"`
	CfOrderNotifyEmails                                                    *string `json:"cf_order_notify_emails"`
	TicketReopened                                                         *string `json:"ticket_reopened"`
	CfExecutionTimeStamp                                                   *string `json:"cf_execution_time_stamp"`
	OverdueReason                                                          *string `json:"overdue_reason"`
	PreviousStatus                                                         *string `json:"previous_status"`
	TypeOfTicket                                                           *string `json:"type_of_ticket"`
	PreviousStartTime                                                      *string `json:"previous_start_time"`
	StatusHistory                                                          *string `json:"status_history"`
	CfOnholdReasons                                                        *string `json:"cf_onhold_reasons"`
	CfRequesterName                                                        *string `json:"cf_requester_name"`
	CfAddress                                                              *string `json:"cf_address"`
	CfAsm                                                                  *string `json:"cf_asm"`
	CfSuh                                                                  *string `json:"cf_suh"`
	CfEscalationQualified                                                  *string `json:"cf_escalation_qualified"`
	CfPythonPaymentOfCancelledOrder                                        *string `json:"cf_python_payment_of_cancelled_order"`
	CfWaitingOnCustomer24Hours                                             *bool   `json:"cf_waiting_on_customer_24_hours"`
	CfCancellationPolicyApplicable                                         *string `json:"cf_cancellation_policy_applicable"`
	CfResolutionPromised                                                   *string `json:"cf_resolution_promised"`
	CfSurveyTriggeredToUbona                                               *bool   `json:"cf_survey_triggered_to_ubona"`
	CfUserID                                                               *string `json:"cf_user_id"`
	CfFsmContactName                                                       *string `json:"cf_fsm_contact_name"`
	CfCloneTicket                                                          *bool   `json:"cf_clone_ticket"`
	CfFsmPhoneNumber                                                       *string `json:"cf_fsm_phone_number"`
	CfFsmServiceLocation                                                   *string `json:"cf_fsm_service_location"`
	CfFsmAppointmentStartTime                                              *string `json:"cf_fsm_appointment_start_time"`
	CfFsmAppointmentEndTime                                                *string `json:"cf_fsm_appointment_end_time"`
	CfCancellationPost20MinsAfterConfirmationFromRestaurantAsSeenInOmsLogs *string `json:"cf_cancellation_post_20_mins_after_confirmation_from_restaurant_as_seen_in_oms_logs"`
	CfHolidaySlotStartDate                                                 *string `json:"cf_holiday_slot_start_date"`
	CfHolidaySlotEndDate                                                   *string `json:"cf_holiday_slot_end_date"`
	CfQualityIssueRestoRecoveryFinalStatus                                 *string `json:"cf_quality_issue_resto_recovery_final_status"`
	CfQualityIssueRestoRecoveryName                                        *string `json:"cf_quality_issue_resto_recovery_name"`
	CfQualityIssueRestoRecoveryRecoveredAmount                             *string `json:"cf_quality_issue_resto_recovery_recovered_amount"`
	CfOrderID199210                                                        *int64  `json:"cf_order_id199210"`
	CfCustomerSegment                                                      *string `json:"cf_customer_segment"`
	CfRestaurantImagesLink                                                 *string `json:"cf_restaurant_images_link"`
	CfTypeOfOutlet                                                         *string `json:"cf_type_of_outlet"`
	CfPermission                                                           int64   `json:"cf_permission"`
}

type CustomFieldInput struct {
	NumberOfMenusUpdated                                                   *string `json:"number_of_menus_updated"`
	Reason1                                                                *string `json:"reason1"`
	NumberOfOutlets                                                        *string `json:"number_of_outlets"`
	TypeOfTask                                                             *string `json:"type_of_task"`
	CfExternalPos                                                          *string `json:"cf_external_pos"`
	Cf24HoursWaitingOnCustomer                                             *bool   `json:"cf_24_hours_waiting_on_customer"`
	CfLevel1Disposition                                                    *string `json:"cf_level_1_disposition"`
	CfLevel2Disposition                                                    *string `json:"cf_level_2_disposition"`
	CfLevel3Disposition                                                    *string `json:"cf_level_3_disposition"`
	CfCheckCaseApplicable                                                  *string `json:"cf_check_case_applicable"`
	CfNextSteps                                                            *string `json:"cf_next_steps"`
	CfCustomization                                                        *string `json:"cf_customization"`
	CfDigitizedBy                                                          *string `json:"cf_digitized_by"`
	CfRestaurantPhoneNumber                                                *int64  `json:"cf_restaurant_phone_number"`
	CfDigitizationTimeStamp                                                *string `json:"cf_digitization_time_stamp"`
	CfCatalogResolvedTimeStamp                                             *string `json:"cf_catalog_resolved_time_stamp"`
	CfCatalogStartTime                                                     *string `json:"cf_catalog_start_time"`
	CfRestaurantID                                                         int64   `json:"cf_restaurant_id"`
	CfCityType                                                             *string `json:"cf_city_type"`
	CfAsmMobile                                                            *string `json:"cf_asm_mobile"`
	CfFssaiLicenceValidity                                                 *string `json:"cf_fssai_licence_validity"`
	CfArea                                                                 *string `json:"cf_area"`
	CfNoOfItemsDiscrepancy                                                 *string `json:"cf_no_of_items_discrepancy"`
	CfNoOfAttemptsMadeForAutomation                                        *string `json:"cf_no_of_attempts_made_for_automation"`
	CfAgentAssignmentTime                                                  *string `json:"cf_agent_assignment_time"`
	CfChildRidForPhotoReplication                                          *string `json:"cf_child_rid_for_photo_replication"`
	CfRestaurantOwnerName                                                  *string `json:"cf_restaurant_owner_name"`
	CfRestaurantOwnerPhoneNumber1                                          *string `json:"cf_restaurant_owner_phone_number_1"`
	CfRestaurantManagerName                                                *string `json:"cf_restaurant_manager_name"`
	CfRestaurantManagerPhoneNumber1                                        *string `json:"cf_restaurant_manager_phone_number_1"`
	CfTicketCreationSource                                                 *string `json:"cf_ticket_creation_source"`
	CfRestaurantNotRespondingSeenInOmsLogs                                 *string `json:"cf_restaurant_not_responding_seen_in_oms_logs"`
	CfItemOutOfStockSeenInOmsLogs                                          *string `json:"cf_item_out_of_stock_seen_in_oms_logs"`
	CfRestaurantClosureSeenInOmsLogs                                       *string `json:"cf_restaurant_closure_seen_in_oms_logs"`
	CfDeIDForOrder                                                         *string `json:"cf_de_id_for_order"`
	CfIsSelect                                                             *bool   `json:"cf_is_select"`
	CfTier                                                                 *string `json:"cf_tier"`
	CfNoOfItemsCreated                                                     *string `json:"cf_no_of_items_created"`
	CfOrderID                                                              *string `json:"cf_order_id"`
	CfRestID                                                               *int64  `json:"cf_rest_id"`
	CfGstEnabled                                                           *bool   `json:"cf_gst_enabled"`
	CfOrdersNotifyMobile                                                   *string `json:"cf_orders_notify_mobile"`
	CfNoOfItemsRequestedForCreation                                        *string `json:"cf_no_of_items_requested_for_creation"`
	CfRevisionsAgent                                                       *string `json:"cf_revisions_agent"`
	CfSmName                                                               *string `json:"cf_sm_name"`
	CfPopulateAllFields                                                    *bool   `json:"cf_populate_all_fields"`
	CfSmEmail                                                              *string `json:"cf_sm_email"`
	CfAdditionalRidForWhichSameResolutionIsToBeDone                        *string `json:"cf_additional_rid_for_which_same_resolution_is_to_be_done"`
	CfFssaiDocumentURL                                                     *string `json:"cf_fssai_document_url"`
	CfAsmName                                                              *string `json:"cf_asm_name"`
	RestaurantName                                                         *string `json:"restaurant_name"`
	CfFssaiLicenceNumber                                                   *string `json:"cf_fssai_licence_number"`
	CfAsmEmail                                                             *string `json:"cf_asm_email"`
	CfFullInformationTimeEnd                                               *string `json:"cf_full_information_time_end"`
	CfTypeOfPartner                                                        *int64  `json:"cf_type_of_partner"`
	CfFullInformationTimeStart                                             *string `json:"cf_full_information_time_start"`
	CfRestaurantName                                                       *string `json:"cf_restaurant_name"`
	RtpinternalUseOnly                                                     *string `json:"rtpinternal_use_only"`
	CfAreaCode                                                             *int64  `json:"cf_area_code"`
	CfMultipleRid                                                          *string `json:"cf_multiple_rid"`
	SalesPoc                                                               *string `json:"sales_poc"`
	CfReopenReason                                                         *string `json:"cf_reopen_reason"`
	CfRestaurantCity                                                       *string `json:"cf_restaurant_city"`
	VMPoc                                                                  *string `json:"vm_poc"`
	CfExecutedBy                                                           *string `json:"cf_executed_by"`
	CfCityCode                                                             *int64  `json:"cf_city_code"`
	UnregisteredUserCity                                                   *string `json:"unregistered_user_city"`
	CfSmMobile                                                             *string `json:"cf_sm_mobile"`
	CfFreezeTime                                                           *string `json:"cf_freeze_time"`
	CfOrderNotifyEmails                                                    *string `json:"cf_order_notify_emails"`
	TicketReopened                                                         *string `json:"ticket_reopened"`
	CfExecutionTimeStamp                                                   *string `json:"cf_execution_time_stamp"`
	OverdueReason                                                          *string `json:"overdue_reason"`
	PreviousStatus                                                         *string `json:"previous_status"`
	TypeOfTicket                                                           *string `json:"type_of_ticket"`
	PreviousStartTime                                                      *string `json:"previous_start_time"`
	StatusHistory                                                          *string `json:"status_history"`
	CfOnholdReasons                                                        *string `json:"cf_onhold_reasons"`
	CfRequesterName                                                        *string `json:"cf_requester_name"`
	CfAddress                                                              *string `json:"cf_address"`
	CfAsm                                                                  *string `json:"cf_asm"`
	CfSuh                                                                  *string `json:"cf_suh"`
	CfEscalationQualified                                                  *string `json:"cf_escalation_qualified"`
	CfPythonPaymentOfCancelledOrder                                        *string `json:"cf_python_payment_of_cancelled_order"`
	CfWaitingOnCustomer24Hours                                             *bool   `json:"cf_waiting_on_customer_24_hours"`
	CfCancellationPolicyApplicable                                         *string `json:"cf_cancellation_policy_applicable"`
	CfResolutionPromised                                                   *string `json:"cf_resolution_promised"`
	CfSurveyTriggeredToUbona                                               *bool   `json:"cf_survey_triggered_to_ubona"`
	CfUserID                                                               *string `json:"cf_user_id"`
	CfFsmContactName                                                       *string `json:"cf_fsm_contact_name"`
	CfCloneTicket                                                          *bool   `json:"cf_clone_ticket"`
	CfFsmPhoneNumber                                                       *string `json:"cf_fsm_phone_number"`
	CfFsmServiceLocation                                                   *string `json:"cf_fsm_service_location"`
	CfFsmAppointmentStartTime                                              *string `json:"cf_fsm_appointment_start_time"`
	CfFsmAppointmentEndTime                                                *string `json:"cf_fsm_appointment_end_time"`
	CfCancellationPost20MinsAfterConfirmationFromRestaurantAsSeenInOmsLogs *string `json:"cf_cancellation_post_20_mins_after_confirmation_from_restaurant_as_seen_in_oms_logs"`
	CfHolidaySlotStartDate                                                 *string `json:"cf_holiday_slot_start_date"`
	CfHolidaySlotEndDate                                                   *string `json:"cf_holiday_slot_end_date"`
	CfQualityIssueRestoRecoveryFinalStatus                                 *string `json:"cf_quality_issue_resto_recovery_final_status"`
	CfQualityIssueRestoRecoveryName                                        *string `json:"cf_quality_issue_resto_recovery_name"`
	CfQualityIssueRestoRecoveryRecoveredAmount                             *string `json:"cf_quality_issue_resto_recovery_recovered_amount"`
	CfOrderID199210                                                        *int64  `json:"cf_order_id199210"`
	CfCustomerSegment                                                      *string `json:"cf_customer_segment"`
	CfRestaurantImagesLink                                                 *string `json:"cf_restaurant_images_link"`
	CfTypeOfOutlet                                                         *string `json:"cf_type_of_outlet"`
	CfPermission                                                           *int64  `json:"cf_permission"`
}

type CustomerCount struct {
	Current  int64 `json:"current"`
	Previous int64 `json:"previous"`
}

type CustomerSentiments struct {
	Current  float64 `json:"current"`
	Previous float64 `json:"previous"`
	MaxScore float64 `json:"maxScore"`
	Arrow    string  `json:"arrow"`
}

type DependencyTuple struct {
	Key   string    `json:"key"`
	Value string    `json:"value"`
	Type  *DataType `json:"type"`
}

type FeedbackReq struct {
	UserID    string      `json:"userId"`
	ContentID string      `json:"contentId"`
	Feedback  interface{} `json:"feedback"`
}

type FeedbackResp struct {
	ContentID string      `json:"contentId"`
	Feedback  interface{} `json:"feedback"`
}

type GetNodeResponse struct {
	ID          string        `json:"id"`
	IsLeaf      bool          `json:"is_leaf"`
	Content     []interface{} `json:"content"`
	SubTitle    string        `json:"sub_title"`
	Title       string        `json:"title"`
	Issues      []*Issues     `json:"issues"`
	Permissions int64         `json:"permissions"`
}

type GetSlotRequest struct {
	RestaurantIds []int64 `json:"restaurantIds"`
}

type GetSlotResponse struct {
	RID  int64         `json:"rId"`
	Days []*SlotsByDay `json:"days"`
}

type Issues struct {
	ID           string   `json:"id"`
	IsLeaf       bool     `json:"is_leaf"`
	Dependencies []string `json:"dependencies"`
	Title        string   `json:"title"`
	SubTitle     string   `json:"sub_title"`
}

type LoginRequest struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type LoginResponse struct {
	Restaurants    []*Restaurant `json:"restaurants"`
	ID             int           `json:"ID"`
	Mobile         string        `json:"mobile"`
	Rid            int           `json:"rid"`
	Name           string        `json:"name"`
	City           string        `json:"city"`
	AccessToken    string        `json:"access_token"`
	UserRole       int           `json:"userRole"`
	Permissions    []int         `json:"permissions"`
	ChangePassword bool          `json:"change_password"`
	UserType       string        `json:"userType"`
}

type MenuSessions struct {
	Period   string  `json:"period"`
	Current  int64   `json:"current"`
	Previous int64   `json:"previous"`
	Vendor   *Vendor `json:"vendor"`
	Arrow    string  `json:"arrow"`
}

type Metrics struct {
	CartSessions       *CartSessions              `json:"cartSessions"`
	MenuSessions       *MenuSessions              `json:"menuSessions"`
	NewRepeatCustomer  *NewRepeatCustomer         `json:"newRepeatCustomer"`
	ConversionMetrics  *ConversionMetricsResponse `json:"conversionMetrics"`
	BusinessMetrics    *BusinessMetrics           `json:"businessMetrics"`
	CustomerSentiments *CustomerSentiments        `json:"customerSentiments"`
}

type MetricsData struct {
	RestaurantID           int64  `json:"restaurantId"`
	NeedsAtttentionMetrics string `json:"needsAtttentionMetrics"`
	CurrentScore           int64  `json:"currentScore"`
	PreviousScore          int64  `json:"previousScore"`
	MaxScore               int64  `json:"maxScore"`
	ArrowDirection         string `json:"arrowDirection"`
	CurrentTierName        string `json:"currentTierName"`
	CurrentTierScore       int64  `json:"currentTierScore"`
	PreviousTierScore      int64  `json:"previousTierScore"`
	MaxTierScore           int64  `json:"maxTierScore"`
}

type NewRepeatCustomer struct {
	Period              string         `json:"period"`
	Vendor              *Vendor        `json:"vendor"`
	NewCustomer         *CustomerCount `json:"newCustomer"`
	RepeatCustomer      *CustomerCount `json:"repeatCustomer"`
	NewCustomerArrow    string         `json:"newCustomerArrow"`
	RepeatCustomerArrow string         `json:"repeatCustomerArrow"`
}

type OrderNotification struct {
	RestaurantID int64  `json:"restaurantId"`
	OrderID      int64  `json:"orderId"`
	Event        string `json:"event"`
	TimeStamp    string `json:"timeStamp"`
}

type RestMeta struct {
	ID           *int    `json:"id"`
	RestaurantID *int    `json:"restaurantId"`
	Attribute    *string `json:"attribute"`
	Value        *string `json:"value"`
}

type Restaurant struct {
	RestID       int         `json:"rest_id"`
	RestName     string      `json:"rest_name"`
	AreaID       *int        `json:"area_id"`
	AreaName     string      `json:"area_name"`
	CityID       int         `json:"city_id"`
	CityName     string      `json:"city_name"`
	Locality     string      `json:"locality"`
	Assured      bool        `json:"assured"`
	Rating       *float64    `json:"rating"`
	Enabled      bool        `json:"enabled"`
	RestMetadata []*RestMeta `json:"restMetadata"`
	LatLong      string      `json:"lat_long"`
}

type SessionDetails struct {
	Restaurants []int64 `json:"restaurants"`
	Username    string  `json:"username"`
	Permissions []int   `json:"permissions"`
}

type Slot struct {
	OpenTime  int `json:"open_time"`
	CloseTime int `json:"close_time"`
}

type SlotsByDay struct {
	Day   string  `json:"day"`
	Slots []*Slot `json:"slots"`
}

type Space struct {
	SpaceID        string        `json:"spaceId"`
	SpaceName      string        `json:"spaceName"`
	CollectionName *string       `json:"collectionName"`
	CollectionID   *string       `json:"collectionId"`
	Type           string        `json:"type"`
	Data           []interface{} `json:"data"`
}

type SwiggyStarRewardsRequest struct {
	RestaurantIds []int64 `json:"restaurantIds"`
}

type SwiggyStarRewardsResponse struct {
	Result []*MetricsData `json:"result"`
}

type Ticket struct {
	Attachments     []*Attachment `json:"attachments"`
	CcEmails        []*string     `json:"cc_emails"`
	CompanyID       *int64        `json:"company_id"`
	CustomFields    *CustomField  `json:"custom_fields"`
	Deleted         *bool         `json:"deleted"`
	Description     *string       `json:"description"`
	DescriptionText *string       `json:"description_text"`
	DueBy           *string       `json:"due_by"`
	Email           *string       `json:"email"`
	EmailConfigID   *int64        `json:"email_config_id"`
	FacebookID      *string       `json:"facebook_id"`
	FrDueBy         *string       `json:"fr_due_by"`
	FrEscalated     *bool         `json:"fr_escalated"`
	FwdEmails       []*string     `json:"fwd_emails"`
	GroupID         *int64        `json:"group_id"`
	ID              string        `json:"id"`
	IsEscalated     *bool         `json:"is_escalated"`
	Name            *string       `json:"name"`
	Phone           *string       `json:"phone"`
	Priority        *int64        `json:"priority"`
	ProductID       *int64        `json:"product_id"`
	ReplyCcEmails   []*string     `json:"reply_cc_emails"`
	RequesterID     *int64        `json:"requester_id"`
	ResponderID     *int64        `json:"responder_id"`
	Source          *int64        `json:"source"`
	Spam            *bool         `json:"spam"`
	Status          *int64        `json:"status"`
	Subject         *string       `json:"subject"`
	Tags            []*string     `json:"tags"`
	ToEmails        []*string     `json:"to_emails"`
	TwitterID       *string       `json:"twitter_id"`
	Type            *string       `json:"type"`
	CreatedAt       *string       `json:"created_at"`
	UpdatedAt       *string       `json:"updated_at"`
}

type TicketReply struct {
	BodyText    *string       `json:"body_text"`
	Body        *string       `json:"body"`
	ID          string        `json:"id"`
	UserID      *int64        `json:"user_id"`
	FromEmail   *string       `json:"from_email"`
	CcEmails    []*string     `json:"cc_emails"`
	BccEmails   []*string     `json:"bcc_emails"`
	TicketID    *int64        `json:"ticket_id"`
	RepliedTo   *string       `json:"replied_to"`
	Attachments []*Attachment `json:"attachments"`
	CreatedAt   *string       `json:"created_at"`
	UpdatedAt   *string       `json:"updated_at"`
}

type TicketReplyInput struct {
	Body        *string           `json:"body"`
	Attachments []*graphql.Upload `json:"attachments"`
	FromEmail   *string           `json:"from_email"`
	UserID      *int64            `json:"user_id"`
	CcEmails    []*string         `json:"cc_emails"`
	BccEmails   []*string         `json:"bcc_emails"`
}

type TicketsRes struct {
	Results []*Ticket `json:"results"`
	Total   int64     `json:"total"`
}

type UpdateSlotRequest struct {
	Day   string      `json:"day"`
	Slots interface{} `json:"slots"`
}

type UserDetails struct {
	Restaurants             []*Restaurant `json:"restaurants"`
	Mobile                  string        `json:"mobile"`
	UserRole                int           `json:"userRole"`
	Permissions             []int         `json:"permissions"`
	IsLiveOrder             bool          `json:"isLiveOrder"`
	IsDotIn                 bool          `json:"isDotIn"`
	IsCallPartner           bool          `json:"isCallPartner"`
	IsBillMaskingRestaurant bool          `json:"isBillMaskingRestaurant"`
	IsFssaiAlert            bool          `json:"isFssaiAlert"`
	IsGstAlert              bool          `json:"isGstAlert"`
	IsThirtyMfEnabled       bool          `json:"isThirtyMfEnabled"`
}

type UserResolutionRequest struct {
	UserID  string `json:"userId"`
	SpaceID string `json:"spaceId"`
}

type Vendor struct {
	ID   []int64 `json:"id"`
	Type string  `json:"type"`
}

type DataType string

const (
	DataTypeString DataType = "STRING"
	DataTypeInt    DataType = "INT"
	DataTypeFloat  DataType = "FLOAT"
	DataTypeBool   DataType = "BOOL"
)

var AllDataType = []DataType{
	DataTypeString,
	DataTypeInt,
	DataTypeFloat,
	DataTypeBool,
}

func (e DataType) IsValid() bool {
	switch e {
	case DataTypeString, DataTypeInt, DataTypeFloat, DataTypeBool:
		return true
	}
	return false
}

func (e DataType) String() string {
	return string(e)
}

func (e *DataType) UnmarshalGQL(v interface{}) error {
	str, ok := v.(string)
	if !ok {
		return fmt.Errorf("enums must be strings")
	}

	*e = DataType(str)
	if !e.IsValid() {
		return fmt.Errorf("%s is not a valid DataType", str)
	}
	return nil
}

func (e DataType) MarshalGQL(w io.Writer) {
	fmt.Fprint(w, strconv.Quote(e.String()))
}
