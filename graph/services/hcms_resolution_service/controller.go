package hcms_resolution_service

import (
	"time"

	"bitbucket.org/swigy/vhc-composer/constants"
	"bitbucket.org/swigy/vhc-composer/graph/model"
	"bitbucket.org/swigy/vhc-composer/helper"
	"bitbucket.org/swigy/vhc-composer/metric"
	log "github.com/sirupsen/logrus"
)

type UserContentResolutionServiceRunner interface {
	ResolveByUserIDAndSpaceIDController(sessionKey string, input model.UserResolutionRequest) (*model.Space, error)
	FeedbackController(sessionKey string, input model.FeedbackReq) (*model.FeedbackResp, error)
}

func (svc Service) ResolveByUserIDAndSpaceIDController(sessionKey string, input model.UserResolutionRequest) (*model.Space, error) {
	apiName := "ResolveByUserIDAndSpaceID"
	defer metric.Api.MeasureLatency(apiName, time.Now())
	txn := metric.TraceTransaction(apiName)
	defer metric.CloseTransaction(txn)
	resp, err := svc.ResolveByUserIDAndSpaceID(sessionKey, input, txn)
	metric.Api.IncrementCounter(apiName, err)
	return resp, err
}

func (svc Service) FeedbackController(sessionKey string, input model.FeedbackReq) (*model.FeedbackResp, error) {
	apiName := "Feedback"
	defer metric.Api.MeasureLatency(apiName, time.Now())
	txn := metric.TraceTransaction(apiName)
	defer metric.CloseTransaction(txn)
	_, err := helper.AuthenticateV2(sessionKey, txn)
	if err != nil {
		log.Errorf("Error from AuthV2 API. Error: %v, sessionKey: %v Input: %v", err.Error(), sessionKey, input);
		return nil, helper.LogAndInstrumentError(apiName, constants.AuthorisationFailed)
	}
	resp, err := svc.Feedback(input, txn)
	metric.Api.IncrementCounter(apiName, err)
	return resp, err
}
