package vhc_service

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"bitbucket.org/swigy/vhc-composer/conf"
	"bitbucket.org/swigy/vhc-composer/graph/model"
	"bitbucket.org/swigy/vhc-composer/helper"
	"bitbucket.org/swigy/vhc-composer/metric"
	pb "bitbucket.org/swigy/vhc-service/pkg/proto"
	proto "github.com/golang/protobuf/proto"
	"github.com/newrelic/go-agent/v3/integrations/nrgrpc"
	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/encoding/protojson"
)

type Service struct {
	grpcClient *grpc.ClientConn
}

// GetNode : Fetch and return node from vhc-service using node id & dependecy list
func (vhc Service) GetNode(id string, dependencies map[string]string, accessContext *pb.AccessFilter, ctx context.Context) (*model.GetNodeResponse, error) {
	apiName := "VhcServiceGetNode"
	fields := log.Fields{"API Name": apiName, "Node ID": id, "Dependencies": dependencies, "AccessContext": accessContext}
	log.WithFields(fields).Info("Invoked GetNode")

	c := pb.NewVhcServiceClient(vhc.grpcClient)
	ctx, cancel := context.WithTimeout(ctx, time.Second)
	defer cancel()
	startTime := time.Now()
	resp, err := c.GetNode(ctx, &pb.GetNodeRequest{Id: id, Dependencies: dependencies, AccessContext: accessContext})
	metric.ExternalApi.MeasureLatency(apiName, startTime)
	if err != nil {
		var message string
		var statusCode int
		if e, ok := status.FromError(err); ok {
			switch e.Code() {
			case codes.Internal:
				message = "Internal server"
				statusCode = http.StatusInternalServerError
			case codes.InvalidArgument:
				message = "Invalid argument"
				statusCode = http.StatusUnprocessableEntity
			case codes.NotFound:
				message = "Node not found"
				statusCode = http.StatusNotFound
			default:
				message = "Failed to get node"
				statusCode = http.StatusInternalServerError
			}
		} else {
			message = "Internal server error"
			statusCode = http.StatusInternalServerError
		}
		metric.ExternalApi.IncrementFailureCounter(apiName, message)
		fields["Error Message"] = message
		log.WithError(err).WithFields(fields).Errorf("Error Making gRPC request to VHC Service")

		return nil, helper.Error(message, statusCode)
	}
	log.Infof("[vhc-service GetNode] gRPC Response : %v", resp)
	response, err := unmarshell(resp)
	metric.ExternalApi.IncrementCounter(apiName, err)

	return response, err
}

func unmarshell(data *pb.GetNodeResponse) (*model.GetNodeResponse, error) {

	var nodeResponse model.GetNodeResponse

	nodeResponse.ID = data.Id
	nodeResponse.IsLeaf = data.IsLeaf
	nodeResponse.SubTitle = data.SubTitle
	nodeResponse.Title = data.Title
	nodeResponse.Permissions = data.Permissions
	log.Infof("Input Issues: %v", data.Issues)
	issues, err := json.Marshal(data.Issues)
	if err != nil {
		log.Errorf("Error Marshelling Issues (%v) :%v", data.Issues, err)
		return nil, fmt.Errorf("Error Marshelling Issues")
	}
	err = json.Unmarshal(issues, &nodeResponse.Issues)
	if err != nil {
		log.Errorf("Error Unmarshelling(%v) :%v", data.Issues, err)
		return nil, fmt.Errorf("Error Unmarshelling Issues")
	}
	log.Infof("Output Issues: %v", nodeResponse.Issues)

	var content []interface{}
	for _, val := range data.Content {
		var contentJson interface{}
		ma := protojson.MarshalOptions{
			UseProtoNames: true,
		}
		message := proto.MessageV2(val)
		bs, err := ma.Marshal(message)

		if err != nil {
			log.Errorf("Error in marshalling content (%v) :%v", data.Content, err)
			return nil, fmt.Errorf("Error Marshelling content")
		}

		if err := json.Unmarshal(bs, &contentJson); err != nil {
			log.Errorf("Error in unmarshalling content (%v) :%v", data.Content, err)
			return nil, fmt.Errorf("Error unmarshalling content")
		}
		content = append(content, contentJson)
	}

	nodeResponse.Content = content
	log.Infof("Response content %v", nodeResponse)
	return &nodeResponse, nil
}

func NewVHCService() ServiceRunner {
	return &Service{grpcClient: creategRPCClient()}
}

func creategRPCClient() *grpc.ClientConn {
	vhcServiceURL := conf.VhcServiceURL
	conn, err := grpc.Dial(
		vhcServiceURL,
		grpc.WithInsecure(),
		grpc.WithUnaryInterceptor(nrgrpc.UnaryClientInterceptor),
	)
	if err != nil {
		log.Fatalf("Fail to dial vhc-service: %v", err)
	}
	return conn
}
