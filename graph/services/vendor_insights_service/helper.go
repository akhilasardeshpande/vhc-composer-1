package vendor_insights_service

import (
	"sync"

	"bitbucket.org/swigy/vhc-composer/conf"
	"bitbucket.org/swigy/vhc-composer/constants"
	"bitbucket.org/swigy/vhc-composer/graph/model"
	"bitbucket.org/swigy/vhc-composer/metric"
	"bitbucket.org/swigy/vhc-composer/utils"
	"github.com/newrelic/go-agent/v3/newrelic"
	log "github.com/sirupsen/logrus"
)

type Service struct{}

func GetVendorInsightsService() ServiceRunner {
	return &Service{}
}

//This API fetches all the metrics which are defined for business overview widget
func (svc Service) Metrics(period string, restIds []int64, txn *newrelic.Transaction) (*model.Metrics, error) {
	var response model.Metrics
	var waitGroup sync.WaitGroup
	//parallelize the requests using go-routine and channel i.e.
	//send concurrent API calls to Vendor-Insights for Business Metrics
	metricsCount := 6
	waitGroup.Add(metricsCount)

	//spin off go-routine to fetch CartSessions
	go func() {
		var err error
		defer waitGroup.Done()
		response.CartSessions, err = svc.CartSessions(period, restIds, txn)
		if err != nil {
			response.CartSessions = &model.CartSessions{}
		}
	}()

	//spin off go-routine to fetch Menu-Sessions
	go func() {
		var err error
		defer waitGroup.Done()
		response.MenuSessions, err = svc.MenuSessions(period, restIds, txn)
		if err != nil {
			response.MenuSessions = &model.MenuSessions{}
		}
	}()

	//spin off go-routine to fetch NewRepeatCustomer-Metrics
	go func() {
		var err error
		defer waitGroup.Done()
		response.NewRepeatCustomer, err = svc.NewRepeatCustomer(period, restIds, txn)
		if err != nil {
			response.NewRepeatCustomer = &model.NewRepeatCustomer{}
		}
	}()

	//spin off go-routine to fetch
	go func() {
		var err error
		defer waitGroup.Done()
		response.ConversionMetrics, err = svc.ConversionMetrics(period, restIds, txn)
		if err != nil {
			response.ConversionMetrics = &model.ConversionMetricsResponse{}
		}
	}()

	//spin off go-routine to fetch business metrics
	go func() {
		var err error
		defer waitGroup.Done()
		response.BusinessMetrics, err = svc.BusinessMetrics(period, restIds, txn)
		if err != nil {
			response.BusinessMetrics = &model.BusinessMetrics{}
		}
	}()

	//spin off go-routine to fetch customer sentiments
	go func() {
		var err error
		defer waitGroup.Done()
		response.CustomerSentiments, err = svc.CustomerSentiments(period, restIds, txn)
		if err != nil {
			response.CustomerSentiments = &model.CustomerSentiments{}
		}
	}()

	waitGroup.Wait()

	log.Infof("Metrics fetched: %v", response)
	return &response, nil
}

func (svc Service) CartSessions(period string, restIds []int64, txn *newrelic.Transaction) (*model.CartSessions, error) {
	var url string
	apiName := "CartSessions"
	url = conf.VIBaseURL + conf.VICartSessionsPath
	//set the header
	requestHeader := make(map[string]string)
	requestHeader[constants.ContentType] = constants.ContentVal

	//Set Query params
	restIdString := utils.ConvertToStringFromList(restIds)
	queryParams := make(map[string]string)
	queryParams[constants.PeriodConst] = period
	queryParams[constants.RestIdsConst] = restIdString

	var content CartConversionsResponse
	err := utils.MakeRequest(constants.HttpGetRequest, url, nil, requestHeader, queryParams, &content, apiName, txn)
	if err != nil {
		log.Infof("Error in cart session: %v", err)
		metric.ExternalApi.IncrementFailureCounter(apiName, constants.ReqFailed)
		return nil, err
	}

	if content.StatusCode != constants.SuccessStatusCode {
		metric.ExternalApi.IncrementFailureCounter(apiName, content.StatusMessage)
	}
	log.Infof("CartConversion Response from VI: %v\nRest Ids: %v", content.Data, restIdString)
	metric.ExternalApi.IncrementSuccessCounter(apiName)
	return &content.Data, nil
}

func (svc Service) MenuSessions(period string, restIds []int64, txn *newrelic.Transaction) (*model.MenuSessions, error) {
	var url string
	apiName := "MenuSessions"
	url = conf.VIBaseURL + conf.VIMenuSessionsPath
	//set the header
	requestHeader := make(map[string]string)
	requestHeader[constants.ContentType] = constants.ContentVal

	//Set Query params
	restIdString := utils.ConvertToStringFromList(restIds)
	queryParams := make(map[string]string)
	queryParams[constants.PeriodConst] = period
	queryParams[constants.RestIdsConst] = restIdString

	var content MenuSessionsResponse

	err := utils.MakeRequest(constants.HttpGetRequest, url, nil, requestHeader, queryParams, &content, apiName, txn)
	if err != nil {
		log.Infof("Error in menu session: %v", err)
		metric.ExternalApi.IncrementFailureCounter(apiName, constants.ReqFailed)
		return nil, err
	}

	if content.StatusCode != constants.SuccessStatusCode {
		metric.ExternalApi.IncrementFailureCounter(apiName, content.StatusMessage)
	}

	log.Infof("MenuSession Response from VI: %v\nRest ids: %v", content.Data, restIdString)
	metric.ExternalApi.IncrementSuccessCounter(apiName)

	return &content.Data, nil
}

func (svc Service) NewRepeatCustomer(period string, restIds []int64, txn *newrelic.Transaction) (*model.NewRepeatCustomer, error) {
	var url string
	apiName := "NewRepeatCustomer"
	url = conf.VIBaseURL + conf.VINewRepeatCustomerPath
	//set the header
	requestHeader := make(map[string]string)
	requestHeader[constants.ContentType] = constants.ContentVal

	//Set Query params
	restIdString := utils.ConvertToStringFromList(restIds)
	queryParams := make(map[string]string)
	queryParams[constants.PeriodConst] = period
	queryParams[constants.RestIdsConst] = restIdString

	var content NewRepeatCustomersResponse
	err := utils.MakeRequest(constants.HttpGetRequest, url, nil, requestHeader, queryParams, &content, apiName, txn)
	if err != nil {
		log.Infof("Error in new repeat customer: %v", err)
		metric.ExternalApi.IncrementFailureCounter(apiName, constants.ReqFailed)
		return nil, err
	}

	if content.StatusCode != constants.SuccessStatusCode {
		metric.ExternalApi.IncrementFailureCounter(apiName, content.StatusMessage)
	}

	log.Infof("NewRepeatCustomers Response from VI: %v\nRest ids: %v", content.Data, restIdString)
	metric.ExternalApi.IncrementSuccessCounter(apiName)

	return &content.Data, nil
}

func (svc Service) ConversionMetrics(period string, restIds []int64, txn *newrelic.Transaction) (*model.ConversionMetricsResponse, error) {
	var url string
	apiName := "ConversionMetrics"
	url = conf.VIBaseURL + conf.VIConversionMetricsPath
	//set the header
	requestHeader := make(map[string]string)
	requestHeader[constants.ContentType] = constants.ContentVal

	//Set Query params
	restIdString := utils.ConvertToStringFromList(restIds)
	queryParams := make(map[string]string)
	queryParams[constants.PeriodConst] = period
	queryParams[constants.RestIdsConst] = restIdString

	var content ConversionMetricsResponse
	err := utils.MakeRequest(constants.HttpGetRequest, url, nil, requestHeader, queryParams, &content, apiName, txn)
	if err != nil {
		log.Infof("Error in conversion metrics: %v", err)
		metric.ExternalApi.IncrementFailureCounter(apiName, constants.ReqFailed)
		return nil, err
	}

	if content.StatusCode != constants.SuccessStatusCode {
		metric.ExternalApi.IncrementFailureCounter(apiName, content.StatusMessage)
	}

	log.Infof("OrderConversion Response from VI: %v\nRest ids: %v", content.Data, restIdString)
	metric.ExternalApi.IncrementSuccessCounter(apiName)

	return &content.Data, nil
}

func (svc Service) BusinessMetrics(period string, restIds []int64, txn *newrelic.Transaction) (*model.BusinessMetrics, error) {
	apiName := "BusinessMetrics"
	url := conf.VIBaseURL + conf.VIBusinessMetricsPath

	//set the header
	requestHeader := make(map[string]string)
	requestHeader[constants.ContentType] = constants.ContentVal

	//Set Query params
	restIdString := utils.ConvertToStringFromList(restIds)
	queryParams := make(map[string]string)
	queryParams[constants.PeriodConst] = period
	queryParams[constants.RestIdsConst] = restIdString

	var content BusinessMetricsResponse

	err := utils.MakeRequest(constants.HttpGetRequest, url, nil, requestHeader, queryParams, &content, apiName, txn)
	if err != nil {
		log.Infof("Error in business metrics: %v", err)
		metric.ExternalApi.IncrementFailureCounter(apiName, constants.ReqFailed)
		return nil, err
	}

	if content.StatusCode != constants.SuccessStatusCode {
		metric.ExternalApi.IncrementFailureCounter(apiName, content.StatusMessage)
	}

	log.Infof("Business Metrics Response from VI: %v\nRest ids: %v", content.Data, restIdString)
	metric.ExternalApi.IncrementSuccessCounter(apiName)

	return &content.Data, nil
}

func (svc Service) CustomerSentiments(period string, restIds []int64, txn *newrelic.Transaction) (*model.CustomerSentiments, error) {
	apiName := "CustomerSentiments"
	url := conf.VIBaseURL + conf.VICustomerSentimentsPath

	//set the header
	requestHeader := make(map[string]string)
	requestHeader[constants.ContentType] = constants.ContentVal

	//Set Query params
	restIdString := utils.ConvertToStringFromList(restIds)
	queryParams := make(map[string]string)
	queryParams[constants.PeriodConst] = period
	queryParams[constants.RestIdsConst] = restIdString

	var content CustomerSentimentsResponse

	err := utils.MakeRequest(constants.HttpGetRequest, url, nil, requestHeader, queryParams, &content, apiName, txn)
	if err != nil {
		log.Infof("Error in customer sentiments: %v", err)
		metric.ExternalApi.IncrementFailureCounter(apiName, constants.ReqFailed)
		return nil, err
	}

	if content.StatusCode != constants.SuccessStatusCode {
		metric.ExternalApi.IncrementFailureCounter(apiName, content.StatusMessage)
	}

	log.Infof("Customer Sentiments Response from VI: %v\n Rest ids: %v", content.Data, restIdString)
	metric.ExternalApi.IncrementSuccessCounter(apiName)

	return &content.Data, nil
}
