package srs_service

import (
	"bitbucket.org/swigy/vhc-composer/constants"
	"bitbucket.org/swigy/vhc-composer/graph/model"
	"bitbucket.org/swigy/vhc-composer/helper"
	"bitbucket.org/swigy/vhc-composer/metric"
	"bitbucket.org/swigy/vhc-composer/utils"
	"context"
	log "github.com/sirupsen/logrus"
	"net/http"
	"time"
)

type ServiceRunner interface {
	UpdateTimeSlotsController(ctx context.Context, restaurantID int, request []*model.UpdateSlotRequest) ([]*model.SlotsByDay, error)
	GetTimeSlotController(ctx context.Context, input model.GetSlotRequest) ([]*model.GetSlotResponse, error)
}

func (srs Service) UpdateTimeSlotsController(ctx context.Context, restaurantID int, request []*model.UpdateSlotRequest) ([]*model.SlotsByDay, error) {
	apiName := "UpdateTimeSlots"
	requestID := utils.GetUUIDV4()
	defer metric.Api.MeasureLatency(apiName, time.Now())
	txn := metric.TraceTransaction(apiName)
	defer metric.CloseTransaction(txn)

	sessionKey := ctx.Value(constants.AccessToken).(string)

	logFields := log.Fields{
		"requestID":     requestID,
		"Restaurant Id": restaurantID,
		"api":           apiName,
	}
	ctx = context.WithValue(ctx, "logFields", logFields)
	ctx = context.WithValue(ctx, "txn", txn)
	user, err := helper.AuthenticateWithRestaurantsV3(ctx, sessionKey, []int64{int64(restaurantID)}, 0)
	if err != nil {
		return nil, helper.LogAndInstrumentErrorV2(apiName, err.Error(), http.StatusUnauthorized, logFields)
	}
	resp, err := srs.UpdateSlots(ctx, user.Username, restaurantID, request)
	if err != nil {
		return nil, helper.LogAndInstrumentErrorV2(apiName, err.Error(), http.StatusBadRequest, logFields)
	}
	metric.Api.IncrementSuccessCounter(apiName)
	return resp, err
}

func (srs Service) GetTimeSlotController(ctx context.Context, input model.GetSlotRequest) ([]*model.GetSlotResponse, error) {
	apiName := "GetTimeSlots"
	requestID := utils.GetUUIDV4()
	defer metric.Api.MeasureLatency(apiName, time.Now())
	txn := metric.TraceTransaction(apiName)
	defer metric.CloseTransaction(txn)

	sessionKey := ctx.Value(constants.AccessToken).(string)
	logFields := log.Fields{
		"requestID":     requestID,
		"restaurantIds": input.RestaurantIds,
		"api":           apiName,
	}
	ctx = context.WithValue(ctx, "logFields", logFields)
	ctx = context.WithValue(ctx, "txn", txn)
	_, err := helper.AuthenticateWithRestaurantsV3(ctx, sessionKey, input.RestaurantIds, 0)
	if err != nil {
		return nil, helper.LogAndInstrumentErrorV2(apiName, err.Error(), http.StatusUnauthorized, logFields)
	}
	resp, err := srs.GetSlots(ctx, input.RestaurantIds)
	if err != nil {
		return nil, helper.LogAndInstrumentErrorV2(apiName, err.Error(), http.StatusBadRequest, logFields)
	}
	metric.Api.IncrementSuccessCounter(apiName)
	return resp, err
}
