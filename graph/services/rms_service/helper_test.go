package rms_service

import (
	"io/ioutil"
	"testing"

	"bitbucket.org/swigy/vhc-composer/conf"
	"github.com/jarcoal/httpmock"
	"github.com/stretchr/testify/require"
)

func TestLogin(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	URL := conf.RmsURL + conf.RmsLoginPath
	mockData, err := ioutil.ReadFile("../../../mocks/data/rms_login.json")
	if err != nil {
		t.Errorf("Failed to read mock data: %v", err)
	}
	httpmock.RegisterResponder("POST", URL,
		httpmock.NewBytesResponder(200, mockData))

	res, err := Service{}.Login("12895", "dummyPass", nil)
	if err != nil {
		t.Error(err)
	}

	require.Equal(t, "332ee070-e74d-47fe-8b91-13ef5c54feca", res.AccessToken, "Access token doesn't match with mock data")
	require.Equal(t, []int{1, 2, 3, 4, 5}, res.Permissions, "Permissions doesn't match with mock data")
}

func TestLogout(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	URL := conf.RmsURL + conf.RmsLogoutPath
	mockData, err := ioutil.ReadFile("../../../mocks/data/rms_logout.json")
	if err != nil {
		t.Errorf("Failed to read mock data: %v", err)
	}
	httpmock.RegisterResponder("POST", URL,
		httpmock.NewBytesResponder(200, mockData))

	res, err := Service{}.Logout("12895", "dummySession", nil)
	if err != nil {
		t.Error(err)
	}
	require.Equal(t, true, res, "response doesn't match with mock data")
}

func TestGetUser(t *testing.T) {

	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	URL := conf.RmsURL + conf.RmsUserInfoPath
	mockData, err := ioutil.ReadFile("../../../mocks/data/rms_get_user.json")
	if err != nil {
		t.Errorf("Failed to read mock data: %v", err)
	}
	httpmock.RegisterResponder("GET", URL,
		httpmock.NewBytesResponder(200, mockData))

	res, err := Service{}.GetUser("dummySession", nil)
	if err != nil {
		t.Error(err)
	}
	require.Equal(t, 12895, res.Restaurants[0].RestID, "RestID doesn't match with mock data")

}
