package rms_service

import (
	"time"

	"bitbucket.org/swigy/vhc-composer/graph/model"
	"bitbucket.org/swigy/vhc-composer/metric"
	log "github.com/sirupsen/logrus"
)

type ServiceRunner interface {
	LoginController(username string, password string) (*model.LoginResponse, error)
	LogoutController(username, sessionKey string) (bool, error)
	GetUserController(sessionKey string) (*model.UserDetails, error)
	GetSessionInfoController(sessionKey string) (*model.SessionDetails, error)
}

// LoginController : Authenticates the user in RMS & returns access_token along with some user info
func (rms Service) LoginController(username string, password string) (*model.LoginResponse, error) {
	apiName := "Login"
	defer metric.Api.MeasureLatency(apiName, time.Now())
	txn := metric.TraceTransaction(apiName)
	defer metric.CloseTransaction(txn)
	resp, err := rms.Login(username, password, txn)
	log.Infof("[rms_service.Login] Response: %v Error: %v", resp, err)
	metric.Api.IncrementCounter(apiName, err)

	return resp, err
}

// LogoutController : Invalidates the session key of the user in RMS & returns a bool with true indicating success
func (rms Service) LogoutController(username, sessionKey string) (bool, error) {
	apiName := "Logout"
	defer metric.Api.MeasureLatency(apiName, time.Now())
	txn := metric.TraceTransaction(apiName)
	defer metric.CloseTransaction(txn)
	resp, err := rms.Logout(username, sessionKey, txn)
	log.Infof("[rms_service.Logout] Response: %v Error: %v", resp, err)
	metric.Api.IncrementCounter(apiName, err)

	return resp, err
}

// GetUserController : Using session key it fetch & returns the user info from RMS
func (rms Service) GetUserController(sessionKey string) (*model.UserDetails, error) {
	apiName := "GetUser"
	defer metric.Api.MeasureLatency(apiName, time.Now())
	txn := metric.TraceTransaction(apiName)
	defer metric.CloseTransaction(txn)
	resp, err := rms.GetUser(sessionKey, txn)
	log.Infof("[rms_service.GetUser] Response: %v Error: %v", resp, err)
	metric.Api.IncrementCounter(apiName, err)

	return resp, err
}

// GetSessionInfoController : Using session key it fetch & returns the user info from RMS
func (rms Service) GetSessionInfoController(sessionKey string) (*model.SessionDetails, error) {
	apiName := "GetSessionInfo"
	defer metric.Api.MeasureLatency(apiName, time.Now())
	txn := metric.TraceTransaction(apiName)
	defer metric.CloseTransaction(txn)
	resp, err := rms.GetSessionInfo(sessionKey, txn)
	log.Infof("[rms_service.GetUser] Response: %v Error: %v", resp, err)
	metric.Api.IncrementCounter(apiName, err)

	return resp, err
}
