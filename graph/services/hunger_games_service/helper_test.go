package hunger_games_service

import (
	"io/ioutil"
	"testing"

	"bitbucket.org/swigy/vhc-composer/conf"
	"github.com/jarcoal/httpmock"
	"github.com/stretchr/testify/require"
)

func TestSwiggyStarRewards(t *testing.T) {
	httpmock.Activate()
	defer httpmock.Deactivate()

	scoreMockData, err := ioutil.ReadFile("../../../mocks/data/score_response.json")
	require.Equal(t, nil, err, "Could not fetch Score mock data")
	httpmock.RegisterResponder("GET", conf.HungerGamesBaseURL+conf.HungerGamesGetScorePath,
		httpmock.NewBytesResponder(200, scoreMockData))

	tierMockData, err := ioutil.ReadFile("../../../mocks/data/tier_response.json")
	require.Equal(t, nil, err, "Could not fetch Tier mock data")
	httpmock.RegisterResponder("GET", conf.HungerGamesBaseURL+conf.HungerGamesGetTieringDetailsPath,
		httpmock.NewBytesResponder(200, tierMockData))

	restIds := []int64{100}

	_, err = Service{}.SwiggyStarRewards(restIds, nil)
	require.Equal(t, nil, err, "failed to Swiggy Star Rewards")

}
