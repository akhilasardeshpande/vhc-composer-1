package helper

import (
	"bitbucket.org/swigy/vhc-composer/constants"
	useragent "github.com/mileusna/useragent"
	log "github.com/sirupsen/logrus"
)

// DetectPlatform takes user agent string and returns the platform (android, ios, web)
func DetectPlatform(userAgent string) string {
	var platform string

	ua := useragent.Parse(userAgent)
	switch {
	case ua.IsAndroid():
		platform = constants.Android.String()
	case ua.IsIOS():
		platform = constants.IOS.String()
	default:
		platform = constants.Web.String()
	}

	logFields := log.Fields{
		"User Agent (input)":  userAgent,
		"User Agent (Parsed)": ua,
	}
	log.WithFields(logFields).Infof("Platform: %s", platform)

	return platform
}
