package helper

import (
	"encoding/json"

	log "github.com/sirupsen/logrus"
)

// HandleGqItemID : TODO Doc
func HandleGqItemID(i interface{}, v interface{}) (err error) {
	respData, err := json.Marshal(i)
	if err != nil {
		log.Error(err)
		return err
	}

	logFields := log.Fields{"data": string(respData)}

	var tempResp map[string]interface{}
	json.Unmarshal(respData, &tempResp)
	delete(tempResp, "id")

	tempData, err := json.Marshal(tempResp)
	if err != nil {
		log.WithFields(logFields).Error(err)
		return err
	}

	if err := json.Unmarshal(tempData, v); err != nil {
		log.WithFields(logFields).Error(err)
		return err
	}

	return nil
}
