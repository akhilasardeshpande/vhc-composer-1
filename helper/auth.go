package helper

import (
	"bitbucket.org/swigy/vhc-composer/conf"
	"bitbucket.org/swigy/vhc-composer/constants"
	"bitbucket.org/swigy/vhc-composer/graph/model"
	"bitbucket.org/swigy/vhc-composer/metric"
	"bitbucket.org/swigy/vhc-composer/types"
	"bitbucket.org/swigy/vhc-composer/utils"
	"context"
	"fmt"
	"github.com/gojek/heimdall/v7/httpclient"
	"github.com/newrelic/go-agent/v3/newrelic"
	log "github.com/sirupsen/logrus"
	"net/http"
	"time"
)

func Authenticate(ctx context.Context, sessionKey string) (*model.SessionDetails, error) {
	logFields := log.Fields{constants.RequestID: utils.GetRequestIdFromContext(ctx)}
	log.WithFields(logFields).Info("Authenticate invoked")
	user, err := GetSessionInfo(ctx, sessionKey)
	if err == nil {
		log.WithFields(logFields).Infof("User: %v", user)
	}

	return user, err
}

func AuthenticateV2(sessionKey string, txn *newrelic.Transaction) (*model.SessionDetails, error) {
	reqId := utils.GetUUIDV4()
	logFields := log.Fields{constants.RequestID: reqId}
	log.WithFields(logFields).Info("Authenticate invoked")
	ctx := newrelic.NewContext(context.TODO(), txn)
	ctx = utils.AddRequestIdToContext(ctx, reqId)
	user, err := GetSessionInfo(ctx, sessionKey)

	if err != nil {
		log.WithFields(logFields).WithError(err).Errorf("Error from RMS AuthV2 API. Error: %v", err.Error())
		return nil, fmt.Errorf("Invalid Session")
	}
	log.WithFields(logFields).Infof("User: %v", user)

	return user, nil
}

func AuthenticateWithRestaurantsV2(sessionKey string, restaurantIDs []int64, txn *newrelic.Transaction, permission int64) (*model.SessionDetails, error) {
	reqId := utils.GetUUIDV4()
	ctx := newrelic.NewContext(context.TODO(), txn)
	ctx = utils.AddRequestIdToContext(ctx, reqId)
	logFields := log.Fields{"Restaurant IDs": restaurantIDs, constants.RequestID: reqId}

	user, err := Authenticate(ctx, sessionKey)
	if err != nil {
		return nil, fmt.Errorf(constants.AuthorisationFailed)
	}
	err = AuthoriseRestAndPermissionsV2(nil, user, restaurantIDs, permission)
	log.WithFields(logFields).Infof("User: %v", user)

	return user, err
}

// AuthoriseRestAndPermissionsV2 returns error if user doesn't have the permission or access to the restaurants
func AuthoriseRestAndPermissionsV2(ctx context.Context, user *model.SessionDetails, restaurantIDs []int64, permission int64) error {
	validRestIDs := map[int64]bool{}
	for _, rid := range user.Restaurants {
		validRestIDs[rid] = true
	}
	logFields := log.Fields{
		"Permission (Required)": permission,
		"Permissions (User)":    user.Permissions,
		"Rids (Required)":       restaurantIDs,
		"Rids (User)":           user.Restaurants,
		constants.RequestID:     utils.GetRequestIdFromContext(ctx),
	}
	for _, rid := range restaurantIDs {
		if _, ok := validRestIDs[rid]; !ok {
			log.WithFields(logFields).Warnf("Restaurant ID: %d does not belong to this user", rid)
			return fmt.Errorf(constants.InvalidOrUnauthrisedOutletID)
		}
	}

	if permission == 0 {
		return nil
	}

	for _, p := range user.Permissions {
		if p == int(permission) {
			return nil
		}
	}
	log.WithFields(logFields).Warnf("User doesn't have necessary Permission")
	return fmt.Errorf(constants.AuthorisationFailed)
}

func makeHeaderMap(sessionKey string) map[string]string {
	return map[string]string{
		"Cookie":       "Swiggy_Session-alpha=" + sessionKey,
		"Content-Type": "application/json",
		"accesstoken":  sessionKey,
	}
}

func GetSessionInfo(ctx context.Context, sessionKey string) (*model.SessionDetails, error) {
	apiName := "RMS-GetSession"
	defer metric.Api.MeasureLatency(apiName, time.Now())
	url := conf.RmsURL + conf.RmsSessionInfoPath
	reqId := utils.GetRequestIdFromContext(ctx)
	logFields := log.Fields{constants.RequestID: reqId}
	log.WithFields(logFields).Infof("RMS Request {URL: %v, key: %8s }", url, sessionKey)
	requestHeader := makeHeaderMap(sessionKey)
	var content GetSessionResponse

	client := httpclient.NewClient(
		httpclient.WithRetryCount(constants.RMSMaxRetry),
		httpclient.WithHTTPTimeout(constants.RMSTimeout),
	)

	req := types.HttpRequest{Context: ctx, Url: url, Method: "GET", Headers: requestHeader}
	meta := types.HttpRequestMeta{ApiName: apiName}
	err := utils.MakeAPICall(client, req, &content, meta)
	if err != nil {
		metric.ExternalApi.IncrementFailureCounter(apiName, err.Error())
		log.WithFields(logFields).Errorf("Error from RMS AuthV2 API: %v", err.Error())
		return nil, ErrorWithId(constants.InvalidResponseFromUpstream, http.StatusBadGateway, reqId)
	}
	if content.StatusCode == -3 {
		log.WithFields(logFields).Infof("Invalid session response from RMS. statusMessage: %v, Data: %v", content.StatusMessage, content.Data)
		metric.ExternalApi.IncrementFailureCounter(apiName, content.StatusMessage)
		return nil, Error(constants.AuthorisationFailed, http.StatusUnauthorized)
	}
	if content.StatusCode != 0 {
		metric.ExternalApi.IncrementFailureCounter(apiName, content.StatusMessage)
		log.WithFields(logFields).Errorf("Failed to fetch sessionInfo from RMS. statusMessage: %v, Data: %v, statusCode: %v", content.StatusMessage, content.Data, content.StatusCode)
		return nil, ErrorWithId(constants.InvalidResponseFromUpstream, http.StatusBadGateway, reqId)
	}
	metric.ExternalApi.IncrementSuccessCounter(apiName)

	return &content.Data, nil
}

func GetSessionInfoV2(ctx context.Context, sessionKey string) (*model.SessionDetails, error) {
	apiName := "VendorAuth-GetSession"
	defer metric.Api.MeasureLatency(apiName, time.Now())
	url := conf.VendorAuthURL + conf.VendorAuthGetSessionPath
	reqId := utils.GetRequestIdFromContext(ctx)
	requestHeader := map[string]string{
		"access_token": sessionKey,
		"source":       "vhc-composer",
		"request-id":   reqId,
	}
	logFields := log.Fields{constants.RequestID: reqId}
	var content GetSessionResponse

	client := httpclient.NewClient(
		httpclient.WithRetryCount(constants.VendorAuthMaxRetry),
		httpclient.WithHTTPTimeout(constants.VendorAuthTimeout),
	)

	req := types.HttpRequest{Context: ctx, Url: url, Method: "GET", Headers: requestHeader}
	meta := types.HttpRequestMeta{ApiName: apiName}
	err := utils.MakeAPICall(client, req, &content, meta)
	if err != nil {
		metric.ExternalApi.IncrementFailureCounter(apiName, err.Error())
		log.WithFields(logFields).Errorf("Error making api call to vendor auth: %v", err)
		return nil, ErrorWithId(constants.InvalidResponseFromUpstream, http.StatusBadGateway, reqId)
	}
	if content.StatusCode == -3 {
		metric.ExternalApi.IncrementFailureCounter(apiName, content.StatusMessage)
		return nil, Error(constants.AuthorisationFailed, http.StatusUnauthorized)
	}
	if content.StatusCode != 0 {
		metric.ExternalApi.IncrementFailureCounter(apiName, content.StatusMessage)
		log.WithFields(logFields).Errorf("failed to fetch user info from vendor-auth: %v", content.StatusMessage)
		return nil, ErrorWithId(constants.InvalidResponseFromUpstream, http.StatusBadGateway, reqId)
	}
	metric.ExternalApi.IncrementSuccessCounter(apiName)

	return &content.Data, nil
}

func AuthenticateV3(ctx context.Context, sessionKey string) (*model.SessionDetails, error) {
	logFields := log.Fields{constants.RequestID: utils.GetRequestIdFromContext(ctx)}
	log.WithFields(logFields).Info("Authenticate invoked")
	if sessionKey == "" {
		return nil, Error(constants.AuthorisationFailed, http.StatusBadRequest)
	}
	user, err := GetSessionInfoV2(ctx, sessionKey)
	if err == nil {
		log.WithFields(logFields).Infof("User: %v", user)
	}

	return user, err
}

func AuthenticateWithRestaurantsV3(ctx context.Context, sessionKey string, restaurantIDs []int64, permission int64) (*model.SessionDetails, error) {
	user, err := AuthenticateV3(ctx, sessionKey)
	if err != nil {
		return user, err
	}
	err = AuthoriseRestAndPermissionsV2(ctx, user, restaurantIDs, permission)
	if err != nil {
		return user, Error(err.Error(), http.StatusUnauthorized)
	}
	return user, err
}
