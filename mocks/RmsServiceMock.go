package mocks

import (
	"io/ioutil"
	"testing"

	"bitbucket.org/swigy/vhc-composer/conf"
	"bitbucket.org/swigy/vhc-composer/graph/model"
	"github.com/jarcoal/httpmock"
	"github.com/stretchr/testify/mock"
)

type MockedRmsService struct {
	mock.Mock
}

func (d *MockedRmsService) LoginController(username string, password string) (*model.LoginResponse, error) {
	args := d.Called(username, password)
	return args.Get(0).(*model.LoginResponse), args.Error(1)
}

func (d *MockedRmsService) LogoutController(username, sessionKey string) (bool, error) {
	args := d.Called(username, sessionKey)
	return args.Get(0).(bool), args.Error(1)
}

func (d *MockedRmsService) GetUserController(sessionKey string) (*model.UserDetails, error) {
	args := d.Called(sessionKey)
	return args.Get(0).(*model.UserDetails), args.Error(1)
}
func (d *MockedRmsService) GetSessionInfoController(sessionKey string) (*model.SessionDetails, error) {
	args := d.Called(sessionKey)
	return args.Get(0).(*model.SessionDetails), args.Error(1)
}

func MockGetUser(t *testing.T) {
	URL := conf.RmsURL + conf.RmsUserInfoPath
	mockData, err := ioutil.ReadFile("../mocks/data/rms_get_user.json")
	if err != nil {
		t.Errorf("Failed to read mock data: %v", err)
	}
	httpmock.Activate()
	httpmock.RegisterResponder("GET", URL,
		httpmock.NewBytesResponder(200, mockData))
}
func MockGetSessionSuccessResponse(t *testing.T) {
	URL := conf.RmsURL + conf.RmsSessionInfoPath

	rmsMockFilePath := GetAbsolutePath(t, "mocks/data/rms_get_session_response.json")
	mockData, err := ioutil.ReadFile(rmsMockFilePath)
	if err != nil {
		t.Fatalf("Failed to read mock data: %v ,path : %s", err, rmsMockFilePath)
	}
	httpmock.Activate()
	httpmock.RegisterResponder("GET", URL, httpmock.NewBytesResponder(200, mockData))
}
