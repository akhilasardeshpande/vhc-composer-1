package mocks

import (
	"context"
	"fmt"

	"bitbucket.org/swigy/vhc-composer/graph/model"
	pb "bitbucket.org/swigy/vhc-service/pkg/proto"
	"github.com/stretchr/testify/mock"
)

type MockedVhcService struct {
	mock.Mock
}

func (d *MockedVhcService) GetNode(id string, dependencies map[string]string, accessContext *pb.AccessFilter) (*model.GetNodeResponse, error) {
	args := d.Called(id, dependencies)
	return args.Get(0).(*model.GetNodeResponse), args.Error(1)
}

type UnimplementedVhcServer struct {
}

func (*UnimplementedVhcServer) CreateNodeFn(context.Context, *pb.CreateNodeRequest) (*pb.CreateNodeResponse, error) {
	return nil, fmt.Errorf("method CreateNodeFn not implemented")
}

func (*UnimplementedVhcServer) GetNode(context.Context, *pb.GetNodeRequest) (*pb.GetNodeResponse, error) {
	return nil, fmt.Errorf("method GetNode not implemented")
}

func FetchDummyNode() pb.GetNodeResponse {
	return pb.GetNodeResponse{
		Id:       "0",
		IsLeaf:   false,
		Title:    "Title",
		SubTitle: "SubTitle",
		Issues: []*pb.Issues{
			{
				Id:           "0",
				IsLeaf:       false,
				Title:        "Title",
				SubTitle:     "SubTitle",
				Dependencies: []string{"OUTLET_ID"},
			},
		},
		Content: []*pb.ResponseContent{
			{
				Type: "test",
				Data: []*pb.ResponseData{},
			},
		},
		Permissions: 3,
	}
}
