#!make
include conf/commons.conf
export $(shell sed 's/=.*//' conf/commons.conf)
include conf/dev.conf
export $(shell sed 's/=.*//' conf/dev.conf)
-include .env
export $(shell test -f .env  || sed 's/=.*//' .env)

setup:
	git config --global url.'git@bitbucket.org:'.insteadOf https://bitbucket.org

setup-env:
	include .env
	export $(shell sed 's/=.*//' .env)

test-local:
	go test  ./... -coverprofile=cover.tmp.out -coverpkg=./...
	cat cover.tmp.out | grep -v "generated.go\|Mock.go\|_gen.go" > cover.out
	rm cover.tmp.out
	go tool cover -func=cover.out

test:
	go test  ./...  -coverprofile=cover.out -coverpkg=./... 
	go tool cover -func=cover.out

build:
	go build -mod vendor -v -installsuffix cgo -o vhc-composer .

run: build
	./vhc-composer

dev:
	reflex -s -r '\.go$$' -R '(vendor)\/*' -d fancy make run
