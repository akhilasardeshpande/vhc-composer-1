package utils

import (
	"context"
	"github.com/google/uuid"

	"bitbucket.org/swigy/vhc-composer/constants"
)

// AddRequestIdToContext adds request ID to the context
func AddRequestIdToContext(ctx context.Context, requestID string) context.Context {
	return context.WithValue(ctx, constants.RequestID, requestID)
}

// GetRequestIdFromContext gets request ID from the context & if it's not present in context creates new request id.
func GetRequestIdFromContext(ctx context.Context) string {
	if ctx == nil || ctx.Value(constants.RequestID) == nil {
		return uuid.New().String()
	}

	return ctx.Value(constants.RequestID).(string)
}
