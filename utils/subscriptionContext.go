package utils

import (
	"bitbucket.org/swigy/vhc-composer/constants"
	"context"
)

// SubContext : Subscription context.
type SubContext struct {
	Cancel context.CancelFunc
}

// GetSubContext : Gets the Subscriptions context.
func GetSubContext(ctx context.Context) *SubContext {
	c, _ := ctx.Value(constants.SubContextKey).(*SubContext)
	return c
}
