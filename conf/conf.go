package conf

import (
	"fmt"
	"os"
	"strconv"
	"time"
)

// Get returns value of env variable
// that passed as key to the func.
const (
	location = "Asia/Kolkata"
)

// All available env variables are present in
// conf/commons.conf file
func Get(key string) string {
	if value, exists := os.LookupEnv(key); exists {
		return value
	}
	panic(fmt.Sprintf("Env Variable: %s is not defined", key))
}

func GetInt(key string) int {
	num := Get(key)
	val, err := strconv.Atoi(num)
	if err != nil {
		panic(fmt.Sprintf("Key: %v and Value: %v, error converting to int: %v", key, num, err))
	}
	return val
}

type confValues struct {
	Addr string
}

var (
	// BuildNumber is the bitbucket tag assigned to the current build
	BuildNumber string

	// AppPort is PORT assigned to server
	AppPort string

	// Env is application environment {development, staging, production}
	Env string

	// Addr where server should run
	Addr string

	// CorsAllowedMethods is list of http methods allowed in CORS
	CorsAllowedMethods string

	// CorsAllowedOrigins is list of host allowed in CORS
	CorsAllowedOrigins string

	// EnvType is type of environment, can be integ, production
	EnvType string

	// NewRelicKey is License key for New Relic
	NewRelicKey string

	// NewRelicAppName is App Name
	NewRelicAppName string

	// NewRelicEnabled represents New Relic enabled or not
	NewRelicEnabled bool

	// NewRelicEnableDT represents New Relic Distributed Traing is enabled or not
	NewRelicEnableDT bool

	// SentryDsn is the Data Source Name assigned by Sentry
	SentryDsn string

	// DummyEmailPrefix is the prefix for Dummy email used for creating ticket
	DummyEmailPrefix string

	// DummyEmailSufix is the sufix for Dummy email used for creating ticket
	DummyEmailSufix string

	// Redis is the config of redis database
	Redis *RedisConfig
)

type RedisConfig struct {
	RedisUrl string

	// Password of Redis Database
	RedisPassword string

	// timeout of Redis Connection
	RedisTimeout time.Duration

	RedisDBIndex int

	RedisPoolSize int
}

func isRunningInDockerContainer() bool {
	_, err := os.Stat("/.dockerenv")
	return err == nil
}

func init() {
	BuildNumber = Get("BUILD_NUMBER")
	AppPort = Get("APP_PORT")
	Env = Get("ENV_NAME")
	Addr = fmt.Sprintf(":%s", AppPort)
	CorsAllowedMethods = Get("CORS_ALLOWED_METHODS")
	CorsAllowedOrigins = Get("CORS_ALLOWED_ORIGINS")
	EnvType = Get("ENV_TYPE")
	NewRelicAppName = Get("NEW_RELIC_APP_NAME")
	NewRelicKey = Get("NEW_RELIC_LICENSE_KEY")
	NewRelicEnabled = (Get("NEWRELIC_ENABLED") == "true")
	NewRelicEnableDT = (Get("NEWRELIC_ENABLE_DT") == "true")
	SentryDsn = Get("SENTRY_DSN")
	DummyEmailPrefix = Get("DUMMY_EMAILID_PREFIX")
	DummyEmailSufix = Get("DUMMY_EMAILID_SUFFIX")
	if Env == "development" && !isRunningInDockerContainer() {
		Addr = "127.0.0.1" + Addr
	}
	Redis = &RedisConfig{fmt.Sprintf("%s:%s", Get("REDISSTREAM_PRIMARY_ENDPOINT"), Get("REDISSTREAM_PORT")), Get("REDIS_PASSWORD"), time.Duration(GetInt("REDIS_TIMEOUT")) * time.Millisecond, GetInt("REDIS_DB_INDEX"), GetInt("REDIS_POOL_SIZE")}

}
