package main

import (
	"errors"
	"fmt"
	"net/http"
	"syscall"

	"bitbucket.org/swigy/vhc-composer/cache/big_cache"
	"bitbucket.org/swigy/vhc-composer/conf"
	"bitbucket.org/swigy/vhc-composer/constants"
	"bitbucket.org/swigy/vhc-composer/datastore"
	"bitbucket.org/swigy/vhc-composer/graph"
	"bitbucket.org/swigy/vhc-composer/kafka/consumer"
	"bitbucket.org/swigy/vhc-composer/metric"
	"github.com/99designs/gqlgen/graphql/playground"
	"github.com/go-chi/chi"
	"github.com/onrik/logrus/sentry"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/rs/cors"
	log "github.com/sirupsen/logrus"
)

func init() {
	log.SetFormatter(&log.JSONFormatter{PrettyPrint: conf.Env == constants.Development})
	log.SetReportCaller(true)

	if conf.Env == constants.Production {
		hook, err := sentry.NewHook(sentry.Options{Dsn: conf.SentryDsn, Release: conf.BuildNumber}, log.PanicLevel, log.FatalLevel, log.ErrorLevel)
		if err == nil {
			log.AddHook(hook)
		}
	}
	var rLimit syscall.Rlimit
	err := syscall.Getrlimit(syscall.RLIMIT_NOFILE, &rLimit)
	log.WithError(err).Infof("maximum file descriptor number that can be opened by this process (rlimit) : %v", rLimit)
}

func health(w http.ResponseWriter, req *http.Request) {
	fmt.Fprintf(w, "ok")
}

func main() {
	addr := conf.Addr

	router := chi.NewRouter()
	router.Use(cors.AllowAll().Handler)
	client, err := datastore.NewRedisClient(conf.Redis, conf.Env)
	if !errors.Is(err, nil) {
		log.Panic(err)
	}
	defer client.Close()
	big_cache.InitializeWebsocketCache()

	consumer.StartAllConsumers(client)

	srv := graph.GraphqlHandler(client)

	metric.Registry.MustRegister(prometheus.NewGoCollector())

	router.Handle("/metrics", promhttp.HandlerFor(metric.Registry, promhttp.HandlerOpts{}))
	router.HandleFunc("/health", health)
	router.Handle("/query", srv)
	router.HandleFunc("/", health)
	router.Handle("/internal/playground", playground.Handler("GraphQL playground", "/query"))
	log.Printf("connect to %s/internal/playground for GraphQL playground (Build: %s)", addr, conf.BuildNumber)
	if err := http.ListenAndServe(addr, router); err != nil {
		log.Error(err)
	}
}
